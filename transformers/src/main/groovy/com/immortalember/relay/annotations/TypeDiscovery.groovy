package com.immortalember.relay.annotations

import com.immortalember.relay.discovery.TypeDiscoveryFinalTransform
import com.immortalember.relay.discovery.TypeDiscoveryMappingTransform
import com.immortalember.relay.discovery.TypeDiscoveryTransform
import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.SOURCE)
@Target([ElementType.TYPE])
@GroovyASTTransformationClass(classes = [TypeDiscoveryTransform, TypeDiscoveryMappingTransform, TypeDiscoveryFinalTransform])
@interface TypeDiscovery {
    String value() default ""

    boolean container() default false;
}