package com.immortalember.relay.annotations

import com.immortalember.relay.discovery.GlobalFunctionDiscoveryFinalTransform
import com.immortalember.relay.discovery.GlobalFunctionDiscoveryTransform
import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.SOURCE)
@Target([ElementType.FIELD, ElementType.METHOD])
@GroovyASTTransformationClass(classes = [GlobalFunctionDiscoveryTransform, GlobalFunctionDiscoveryFinalTransform])
@interface GlobalFunctionDiscovery {
    String value() default "" // Function name
    boolean sendAck() default true
    boolean isCommand() default false

    Class<?> returnType() default Object.class
    int returnTypeId() default 0

    ParameterField[] parameters() default []

}
