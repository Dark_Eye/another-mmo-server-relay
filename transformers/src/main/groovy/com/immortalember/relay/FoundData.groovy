package com.immortalember.relay

import com.immortalember.relay.annotations.PacketField
import com.immortalember.relay.annotations.TypeDiscovery
import groovy.io.FileType
import groovy.json.JsonException
import groovy.json.JsonSlurper
import org.codehaus.groovy.ast.ClassHelper
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.FieldNode
import org.json.JSONObject
import org.junit.platform.commons.util.ClassUtils

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.util.concurrent.CopyOnWriteArrayList

class FoundData {
    static JsonSlurper jsonSlurper = new JsonSlurper()

    static Map<Integer, Object> FunctionsMapping = new LinkedHashMap<>()
    static Map<Integer, Object> TypesMapping = new LinkedHashMap<>([
            0: [
                    path : Float.packageName,
                    class: Float.simpleName,
                    name : "float"
            ],
            1: [
                    path : Integer.packageName,
                    class: Integer.simpleName,
                    name : "integer"
            ],
            2: [
                    path : byte[].packageName,
                    class: byte[].simpleName,
                    name : "byte array"
            ],
            3: [
                    path : Object[].packageName,
                    class: Object[].simpleName,
                    name : "strict array"
            ],
            4: [
                    path : List.packageName,
                    class: List.simpleName,
                    name : "mixed array"
            ],
            5: [
                    path : UUID.packageName,
                    class: UUID.simpleName,
                    name : "UUID"
            ],
    ])
    static Map<List<String>, Map<String, Object>> TypeConverters = [
            ([String.name, byte[].name].asImmutable())  : [class: "com.immortalember.relay.Util", func: 'encodeString'],
            ([byte[].name, String.name].asImmutable())  : [class: "com.immortalember.relay.Util", func: 'decodeString'],
            ([boolean.name, Integer.name].asImmutable()): [class: "com.immortalember.relay.Util", func: 'encodeBoolean'],
            ([Integer.name, boolean.name].asImmutable()): [class: "com.immortalember.relay.Util", func: 'decodeBoolean'],
            ([byte.name, Integer.name].asImmutable())   : [cast: true],
            ([char.name, Integer.name].asImmutable())   : [cast: true],
            ([short.name, Integer.name].asImmutable())  : [cast: true],
            ([int.name, Integer.name].asImmutable())    : [cast: true],
            ([long.name, Integer.name].asImmutable())   : [cast: true],
            ([float.name, Float.name].asImmutable())    : [cast: true],
            ([Integer.name, byte.name].asImmutable())   : [cast: true],
            ([Integer.name, char.name].asImmutable())   : [cast: true],
            ([Integer.name, short.name].asImmutable())  : [cast: true],
            ([Integer.name, int.name].asImmutable())    : [cast: true],
            ([Integer.name, long.name].asImmutable())   : [cast: true],
            ([Float.name, float.name].asImmutable())    : [cast: true]
    ]
    static Map<String, Integer> TypeLookUp = new LinkedHashMap<>([
            (Float.name)   : 0,
            (Integer.name) : 1,
            (byte[].name)  : 2,
            (Object[].name): 3,
            (List.name)    : 4,
            (UUID.name)    : 5
    ])
    static Map<String, String> TypeAliases = new LinkedHashMap<>([
            (String.name): (byte[].name),
            (byte.name)  : (Integer.name),
            (char.name)  : (Integer.name),
            (short.name) : (Integer.name),
            (int.name)   : (Integer.name),
            (long.name)  : (Integer.name),
            (boolean.name)  : (Integer.name),
            (float.name) : (Float.name)
    ])

    static List<Closure> TypesCallback = new CopyOnWriteArrayList<>()
    static List<Closure> TypeGeneration = []
    static List<Closure> FunctionGeneration = []

    static String BaseDir = System.getProperty("user.dir")

    static String FunctionsFilePath = BaseDir + "/functions.json"
    static String TypesFilePath = BaseDir + "/types.json"
    static String ExportedFunctionsFilePath = BaseDir + "/exported-functions.json"
    static String ExportedTypesFilePath = BaseDir + "/exported-types.json"

    static File FunctionsFile = new File(FunctionsFilePath)
    static File TypesFile = new File(TypesFilePath)
    static File ExportedFunctionsFile = new File(ExportedFunctionsFilePath)
    static File ExportedTypesFile = new File(ExportedTypesFilePath)

    static int NextTypeId = 10
    static int NextFunctionId = 0

    static boolean finalized = false

    static {
        if (!FunctionsFile.exists()) {
            FunctionsFile.createNewFile()
        }
        if (!TypesFile.exists()) {
            TypesFile.createNewFile()
        }
        if (!ExportedFunctionsFile.exists()) {
            ExportedFunctionsFile.createNewFile()
        }
        if (!ExportedTypesFile.exists()) {
            ExportedTypesFile.createNewFile()
        }

        try {
            if (FunctionsFile.exists()) {
                def functionResult = jsonSlurper.parse(FunctionsFile)

                functionResult.each {
                    int key = Integer.parseInt(it.key as String)
                    NextFunctionId = Math.max(NextFunctionId, key)

                    FunctionsMapping.put(key, it.value)
                }

                NextFunctionId++
            }
        } catch (JsonException e) {
            println(FunctionsFilePath)
            println(e)
        }

        try {
            if (TypesFile.exists()) {
                def typesResult = jsonSlurper.parse(TypesFile)

                typesResult.each {
                    int key = Integer.parseInt(it.key as String)
                    if (key >= 10) {
                        key = NextTypeId = Math.max(NextTypeId, key)
                    }

                    TypesMapping.put(key, it.value)
                    if (key >= 10) {
                        TypeLookUp.put("${it.value.path}.${it.value.class}".toString(), key)
                    }
                }

                NextTypeId = Math.max(10, NextFunctionId + 1)
            }
        } catch (JsonException e) {
            println(TypesFilePath)
            println(e)
        }

        new File("templates").eachFile(FileType.ANY, {
            final GroovyClassLoader loader = new GroovyClassLoader()
            if (it.isFile()) {
                if (it.name.endsWith(".groovy")) {
                    def cl = loader.parseClass(it)
                    GroovyObject z = (GroovyObject) cl.getDeclaredConstructor().newInstance()
                    TypeGeneration.add({ z.type(it) })
                    FunctionGeneration.add({ z.function(it) })
                }
            } else {
                loader.addURL(it.toURI().toURL())
                if (new File(it.absolutePath + "/type.groovy").exists()) {
                    def cl = loader.parseClass(new File(it.absolutePath + "/type.groovy"))
                    GroovyObject z = (GroovyObject) cl.getDeclaredConstructor().newInstance()
                    TypeGeneration.add({ z.type(it) })
                }
                if (new File(it.absolutePath + "/function.groovy").exists()) {
                    def cl = loader.parseClass(new File(it.absolutePath + "/function.groovy"))
                    GroovyObject z = (GroovyObject) cl.getDeclaredConstructor().newInstance()
                    FunctionGeneration.add({ z.function(it) })
                }
            }
        })
    }

    static void registerFunction(data) {
        def found = FunctionsMapping.find {
            it.value.class == data.class && it.value.method == data.method
        }

        if (found != null) {
            FunctionsMapping.put(found.key, data)
        } else {
            FunctionsMapping.put(NextFunctionId++, data)
        }
    }

    static void writeFunctions() {
        FunctionsFile.withWriter {
            it << new JSONObject(FunctionsMapping)
        }
        ExportedFunctionsFile.withWriter {
            it << new JSONObject(FunctionsMapping.collectEntries { k, v ->
                [(k): v.subMap(['name', 'returnType', 'arguments', 'requiresAck'])]
            })
        }
    }

    static void registerType(data) {
        def registered = TypesMapping.find {
            it.value.class == data.class && it.value.path == data.path
        }

        if (registered != null) {
            def found = registered.value
            def newFields = data.fields.findAll { value ->
                Map f = found.fields.find({
                    it.value.name == value.name
                })?.value
                if (f != null) {
                    merge(value, f)
                    return false
                }
                return true
            }
            def newMethods = data.methods.findAll { value ->
                Map f = found.methods.find({
                    it.value.name == value.name
                })?.value
                if (f != null) {
                    merge(value, f)
                    return false
                }
                return true
            }

            int fieldNextId = (data.fields.max { it?.key ?: 0 }?.key ?: 0) + 1
            int methodNextId = (data.methods.max { it?.key ?: 0 }?.key ?: 0) + 1

            newFields.each {
                found.fields.put("" + fieldNextId++, it)
            }
            newMethods.each {
                found.methods.put("" + methodNextId++, it)
            }

            data.fields = found.fields
            data.methods = found.methods

            TypesMapping.put(registered.key, data)
            TypeLookUp.put("${data.path}.${data.class}".toString(), registered.key)
        } else {
            data.fields = data.fields.withIndex().collectEntries { e, i ->
                [(i): e]
            }
            data.methods = data.methods.withIndex().collectEntries { e, i ->
                [(i): e]
            }
            TypesMapping.put(NextTypeId, data)
            TypeLookUp.put("${data.path}.${data.class}".toString(), NextTypeId++)
        }

        TypesCallback.each {
            it(data)
        }
    }

    static void writeTypes() {
        TypesFile.withWriter { it << new JSONObject(TypesMapping) }
    }

    static Map merge(Map... maps) {
        Map result

        if (maps.length == 0) {
            result = [:]
        } else if (maps.length == 1) {
            result = maps[0]
        } else {
            result = [:]
            maps.each { map ->
                map.each { k, v ->
                    result[k] = result[k] instanceof Map ? merge(result[k] as Map, v as Map) : v
                }
            }
        }

        result
    }

    static def findType(ClassNode type) {
        if (type == null) return null
        if (type.array) {
            findType(Object[].name)
        } else {
            findType(type.name)
        }
    }

    static def findType(String type) {
        String name = TypeAliases.getOrDefault(type, type)
        if (TypeLookUp.containsKey(name)) {
            TypeLookUp.get(name)
        } else {
            name
        }
    }

    static String getAlias(String type) {
        TypeAliases.getOrDefault(type, type)
    }

    static synchronized void registerOnTypeCallback(Closure closure) {
        TypesCallback.add(closure)
    }

    static synchronized void unregisterOnTypeCallback(Closure closure) {
        TypesCallback.remove(closure)
    }

    static void finalize() {
        if (finalized) return
        finalized = true

        TypesMapping.each { t ->
            TypeGeneration.each {
                it(t)
            }
        }
        TypeGeneration.each {
            it(false)
        }

        FunctionsMapping.each { f ->
            FunctionGeneration.each {
                it(f)
            }
        }
        FunctionGeneration.each {
            it(false)
        }

        writeTypes()
        writeFunctions()
    }

    public static List<FieldNode> getPacketFields(ClassNode type) {
        List<FieldNode> result = new ArrayList<>()
        List<ClassNode> nodes = [type]

        while (type != null && type.annotations.find {
            it.classNode == ClassHelper.make(TypeDiscovery)
        } != null) {
            nodes.add(type = type.getSuperClass())
        }

        nodes.reverseEach { node ->
            node.fields.each {
                if ((it.annotations.find {
                    it.classNode == ClassHelper.make(PacketField)
                } != null)
                        && !Modifier.isStatic(it.modifiers)
                        && !Modifier.isFinal(it.modifiers)
                        && !it.isSynthetic()) {
                    result.add(it)
                }
            }
        }

        result
    }
}
