package com.immortalember.relay.discovery

import com.immortalember.relay.FoundData
import com.immortalember.relay.annotations.PacketField
import groovyjarjarasm.asm.Opcodes
import org.codehaus.groovy.ast.*
import org.codehaus.groovy.ast.expr.*
import org.codehaus.groovy.ast.tools.GeneralUtils
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.syntax.SyntaxException
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

@GroovyASTTransformation(phase = CompilePhase.INSTRUCTION_SELECTION)
class TypeDiscoveryMappingTransform implements ASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        def (AnnotationNode annotation, ClassNode classNode) = nodes
        try {
            def ma = new MapExpression()
            FoundData.getPacketFields(classNode).withIndex().collect({ it, i ->
                [id: i, field: it]
            }).each {
                FieldNode field = it.field
                boolean needsConversion = FoundData.getAlias(field.type.name) != field.type.name

                def setParameter = new Parameter(ClassHelper.DYNAMIC_TYPE, "newValue")
                Expression setExpression = new VariableExpression(setParameter)
                if (needsConversion) {
                    def converter = FoundData.TypeConverters[([FoundData.getAlias(field.type.name), field.type.name])]
                    if (converter == null) {
                        source.addErrorAndContinue(new SyntaxException("Missing converted for: ${FoundData.getAlias(field.type.name)} to ${field.type.name}", field.lineNumber, field.columnNumber, field.lastLineNumber, field.lastColumnNumber))
                    } else if (converter.cast) {
                        setExpression = GeneralUtils.castX(field.type, setExpression)
                    } else {
                        setExpression = GeneralUtils.callX(ClassHelper.make(converter.class), converter.func, GeneralUtils.args(setExpression))
                    }
                }
                def set = GeneralUtils.closureX(
                        new Parameter[]{setParameter},
                        GeneralUtils.assignS(GeneralUtils.varX(field), setExpression)
                )
                def setScope = new VariableScope()
                setScope.setClassScope(classNode)
                set.setVariableScope(setScope)

                Expression getExpression = GeneralUtils.varX(field)
                if (needsConversion) {
                    def converter = FoundData.TypeConverters[([field.type.name, FoundData.getAlias(field.type.name)])]
                    if (converter == null) {
                        source.addErrorAndContinue(new SyntaxException("Missing converted for: ${field.type.name} to ${FoundData.getAlias(field.type.name)}", field.lineNumber, field.columnNumber, field.lastLineNumber, field.lastColumnNumber))
                    } else if (converter.cast) {
                        getExpression = GeneralUtils.castX(ClassHelper.make(FoundData.getAlias(field.type.name)), getExpression)
                    } else {
                        getExpression = GeneralUtils.callX(ClassHelper.make(converter.class), converter.func, GeneralUtils.args(getExpression))
                    }
                }
                def get = GeneralUtils.closureX(
                        GeneralUtils.returnS(getExpression)
                )
                def getScope = new VariableScope()
                getScope.setClassScope(classNode)
                get.setVariableScope(getScope)

                if(FoundData.findType(field.type) instanceof String) {
                    source.addErrorAndContinue(new SyntaxException("Unknown type: ${field.type.name}", field))
                }

                ma.addMapEntryExpression(
                        GeneralUtils.constX(it.id),
                        new ConstructorCallExpression(
                                ClassHelper.make('com.immortalember.relay.networking.types.TypeFieldWrapper'),
                                GeneralUtils.args(set, get, new ConstantExpression(FoundData.findType(field.type)), new ClassExpression(field.type))
                        )
                )
            }

            classNode.addField("FIELD_MAPPING",
                    Opcodes.ACC_PUBLIC | Opcodes.ACC_SYNTHETIC | Opcodes.ACC_FINAL | Opcodes.ACC_TRANSIENT,
                    ClassHelper.makeWithoutCaching(Map.class, false),
                    ma
            )
        } catch (e) {
            source.errorCollector.addException(e, source)
            throw e
        }
    }
}
