package com.immortalember.relay.discovery

import com.immortalember.relay.FoundData
import com.immortalember.relay.annotations.GlobalFunctionDiscovery
import com.immortalember.relay.annotations.ParameterField
import org.apache.groovy.ast.tools.ClassNodeUtils
import org.codehaus.groovy.ast.ASTNode
import org.codehaus.groovy.ast.AnnotationNode
import org.codehaus.groovy.ast.ClassHelper
import org.codehaus.groovy.ast.ClassNode
import org.codehaus.groovy.ast.MethodNode
import org.codehaus.groovy.ast.expr.AnnotationConstantExpression
import org.codehaus.groovy.ast.expr.ClassExpression
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.syntax.SyntaxException
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

@GroovyASTTransformation(phase = CompilePhase.CANONICALIZATION)
class GlobalFunctionDiscoveryTransform implements ASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        def (AnnotationNode annotation, actor) = nodes

        if (annotation.getClassNode().getTypeClass()?.name == GlobalFunctionDiscovery.name) {
            def value = annotation?.members?.value
            def sendAck = annotation?.members?.sendAck
            def isCommand = annotation?.members?.isCommand
            def returnType = annotation?.members?.returnType?.type as ClassNode
            def parameters = annotation?.members?.parameters
            def returnTypeId = annotation?.members?.returnTypeId
//            println(((annotation as AnnotationNode).getMember("returnType") as ClassExpression)?.type?.name)
//            println(((annotation as AnnotationNode).getMember("returnType") as ClassExpression)?.type?.nameWithoutPackage)
//            println(value)
//            println(returnType)
//            println(annotation?.members)
//            println(annotation?.members?.returnType)

            def resultType = FoundData.findType(returnType)
            if (resultType == null) {
                if (returnTypeId != null) {
                    resultType = returnTypeId.value
                } else if (actor instanceof MethodNode && !actor.voidMethod) {
                    returnType = actor.returnType
                    resultType = FoundData.findType(actor.returnType)
                }
            }

            sendAck = sendAck == null ? true : sendAck.value
            isCommand = isCommand == null ? false : isCommand.value
            value = value ? value.value : actor.name
            Map details = [requiresAck: sendAck, name: value, returnType: resultType, method: actor.name, isCommand: isCommand]

            if (resultType instanceof String) {
                def me
                me = {
                    if (it.path + "." + it.class == details.returnType) {
                        details.returnType = FoundData.findType(details.returnType)
                        FoundData.unregisterOnTypeCallback(me)
                    }
                }
                FoundData.registerOnTypeCallback(me)
            }
            if(returnType?.array) {
                def aType = FoundData.findType(returnType.componentType)

                details.arrayType = aType
                if (aType instanceof String) {
                    def me
                    me = {
                        if (it.path + "." + it.class == details.arrayType) {
                            details.arrayType = FoundData.findType(details.arrayType)
                            FoundData.unregisterOnTypeCallback(me)
                        }
                    }
                    FoundData.registerOnTypeCallback(me)
                }
            }

            Map params = null
            if (actor instanceof MethodNode) {
                if (parameters == null) {
                    params = [:]
                    actor.parameters.eachWithIndex { it, i ->
                        if (i != 0) {
                            def t = FoundData.findType(it.type)
                            String name = it.name
                            def diets = [name: name, typeId: t, typeName: it.type.nameWithoutPackage, originalType: it.type.name]
                            params.put(i, diets)
                            if (t instanceof String) {
                                def me
                                me = {
                                    if (it.path + "." + it.class ==  diets.typeId) {
                                        diets.typeId = FoundData.findType( diets.typeId)
                                        FoundData.unregisterOnTypeCallback(me)
                                    }
                                }
                                FoundData.registerOnTypeCallback(me)
                            }
                        }
                    }
                }
                details.putAll([class: actor.declaringClass.name, actor: false])
            } else {
                details.putAll([class: actor.owner.name, actor: true])
            }

            if (params == null && parameters != null) {
                params = [:]

                parameters.getExpressions().eachWithIndex { it, i ->
                    def mems = (it.value as AnnotationNode).members
                    String sName = mems.name.value
                    ClassExpression sReturnType = mems.type
                    def sReturnTypeId = mems.typeId

                    if (sReturnType == null && sReturnTypeId == null) {
                        source.addErrorAndContinue(new SyntaxException("No type specified", it))
                        return
                    }

                    def t = sReturnTypeId?.value ?: FoundData.findType(sReturnType.type)
                    def diets = [name: sName, typeId: t, typeName: sReturnType.type.nameWithoutPackage, originalType: sReturnType.type.name]
                    params.put(i, diets)
                    if (t instanceof String) {
                        def me
                        me = {
                            if (it.path + "." + it.class == diets.typeId) {
                                diets.typeId = FoundData.findType(diets.typeId)
                                FoundData.unregisterOnTypeCallback(me)
                            }
                        }
                        FoundData.registerOnTypeCallback(me)
                    }
                }
            }
            if (params != null) {
                details.put("parameters", params)
            }

            FoundData.registerFunction(details)
        }
    }
}
