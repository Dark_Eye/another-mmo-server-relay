package com.immortalember.relay.discovery

import com.immortalember.relay.FoundData
import com.immortalember.relay.annotations.PacketField
import com.immortalember.relay.annotations.PacketMethod
import com.immortalember.relay.annotations.TypeDiscovery
import org.codehaus.groovy.ast.*
import org.codehaus.groovy.control.CompilePhase
import org.codehaus.groovy.control.SourceUnit
import org.codehaus.groovy.transform.ASTTransformation
import org.codehaus.groovy.transform.GroovyASTTransformation

@GroovyASTTransformation(phase = CompilePhase.CANONICALIZATION)
class TypeDiscoveryTransform implements ASTTransformation {
    @Override
    void visit(ASTNode[] nodes, SourceUnit source) {
        def (AnnotationNode annotation, ClassNode classNode) = nodes
        String value = annotation?.members?.value?.value

        value = value != null ? value : classNode.nameWithoutPackage

        List<FieldNode> fields = FoundData.getPacketFields(classNode)
        MethodNode[] methods = classNode.methods.findAll {
            it.annotations.find({
                it.classNode.typeClass.name == PacketMethod.name
            }) != null
        }

        def mapping = []
        def details = [
                parent : classNode.superClass.annotations.find {
                    it.classNode.typeClass.name == TypeDiscovery.name
                } == null ? null : FoundData.findType(classNode.superClass),
                parentName: classNode.superClass.annotations.find {
                    it.classNode.typeClass.name == TypeDiscovery.name
                } == null ? null : classNode.superClass.nameWithoutPackage,
                path   : classNode.packageName,
                class  : classNode.nameWithoutPackage,
                name   : value, /*create: [], update: [], read: [], delete: [], */
                methods: methods.collect({
                    def type = FoundData.findType(it.returnType)
                    def t = ['name': it.name, 'type': FoundData.findType(it.returnType), 'original': it.returnType.name]
                    if (type instanceof String) {
                        def me
                        me = {
                            if (it.path + "." + it.class == t.type) {
                                t.type = FoundData.findType(t.type)
                                FoundData.unregisterOnTypeCallback(me)
                            }
                        }
                        FoundData.registerOnTypeCallback(me)
                    }
                    return t
                }),
                fields : fields.collect({ it }).withIndex().collect({FieldNode it, i ->
                    mapping.add([id: i, field: it])
                    def type = FoundData.findType(it.type)
                    def arrayType = it.type.array ? FoundData.findType(it.type.componentType) : null
                    def t = [name: it.name, type: type, class: it.type.nameWithoutPackage, package: it.type.packageName, id: i, arrayType: arrayType, isInherited: it.owner != classNode]
                    if (type instanceof String) {
                        def me
                        me = {
                            if (it.path + "." + it.class == t.type) {
                                t.type = FoundData.findType(t.type)
                                FoundData.unregisterOnTypeCallback(me)
                            }
                        }
                        FoundData.registerOnTypeCallback(me)
                    }
                    if (arrayType instanceof String) {
                        def me
                        me = {
                            if (it.path + "." + it.class == t.arrayType) {
                                t.arrayType = FoundData.findType(t.arrayType)
                                FoundData.unregisterOnTypeCallback(me)
                            }
                        }
                        FoundData.registerOnTypeCallback(me)
                    }
                    return t
                })
        ]

        if (details.parent instanceof String) {
            def me
            me = {
                if (it.path + "." + it.class == details.parent) {
                    details.parente = FoundData.findType(details.parent)
                    FoundData.unregisterOnTypeCallback(me)
                }
            }
            FoundData.registerOnTypeCallback(me)
        }

        MethodNode creation = classNode.getMethod("create", new Parameter[]{new Parameter(ClassHelper.make("com.immortalember.relay.db.IServerIdentifiable"), ""), new Parameter(ClassHelper.make("com.immortalember.relay.packets.ObjectPacket"), "")})
        details.create = creation != null

        FoundData.registerType(details)
    }
}
