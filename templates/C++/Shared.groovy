class Shared {
    def static typeLookupMap = [UUID: 'FGuid', 'UUID[]': 'TArray<FGuid>', byte: 'uint8', String: 'FString', boolean: 'bool']
    def static serailizedLookup = [
            UUID    : 'UNetworkDataParser::EncodeGuid(${it.value.name}, data)',
            float   : 'UNetworkDataParser::EncodeFloat(${it.value.name}, data)',
            int     : 'UNetworkDataParser::EncodeIntegral(${it.value.name}, data)',
            byte    : 'UNetworkDataParser::EncodeIntegral(${it.value.name}, data)',
            short   : 'UNetworkDataParser::EncodeIntegral(${it.value.name}, data)',
            char    : 'UNetworkDataParser::EncodeUnsignedIntegral(${it.value.name}, data)',
            String  : 'UNetworkDataParser::EncodeString(${it.value.name}, data)',
            'UUID[]': 'UNetworkDataParser::EncodeGuidArray(${it.value.name}, data)',
            boolean : 'UNetworkDataParser::EncodeIntegral(${it.value.name} ? 1 : 0, data)'
    ]
    def static deserailizedLookup = [
            UUID    : 'UNetworkDataParser::DecodeGuid(data)',
            float   : 'UNetworkDataParser::DecodeFloat(data)',
            int     : 'UNetworkDataParser::DecodeIntegral(data)',
            byte    : 'UNetworkDataParser::DecodeIntegral(data)',
            short   : 'UNetworkDataParser::DecodeIntegral(data)',
            char    : 'UNetworkDataParser::DecodeUnsignedIntegral(data)',
            String  : 'UNetworkDataParser::DecodeString(data)',
            'UUID[]': 'UNetworkDataParser::DecodeGuidArray(data)',
            boolean : 'UNetworkDataParser::DecodeIntegral(data) != 0'
    ]


    def static lookupType(def type) {
        typeLookupMap.getOrDefault(type, type)
    }
}
