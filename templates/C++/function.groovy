import groovy.transform.SourceURI
import java.nio.file.Path
import java.nio.file.Paths
import groovy.text.GStringTemplateEngine

import static Shared.*

class CppFuncGen {

    @SourceURI
    URI sourceUri
    Path scriptLocation = Paths.get(sourceUri).getParent()

    GStringTemplateEngine engine = new GStringTemplateEngine()
    def functionHeaderTemplate = engine.createTemplate(scriptLocation.resolve("function-header.templ").toFile().text)
    def functionImplementationTemplate = engine.createTemplate(scriptLocation.resolve("function-implementation.templ").toFile().text)
    def functionConsoleHeaderTemplate = engine.createTemplate(scriptLocation.resolve("function-console-header.templ").toFile().text)
    def functionConsoleImplementationTemplate = engine.createTemplate(scriptLocation.resolve("function-console-implementation.templ").toFile().text)

    def folder = new File("generated/c++")
    String removePath = "com.immortalember.relay.db"
    Path header = Paths.get(folder.absolutePath, "NetworkFunctions.h")
    Path definition = Paths.get(folder.absolutePath, "NetworkFunctions.cpp")
    Path headerConsole = Paths.get(folder.absolutePath, "RemoteCheatManager.h")
    Path definitionConsole = Paths.get(folder.absolutePath, "RemoteCheatManager.cpp")

    Map<Integer, Object> functions = [:]

    CppFuncGen() {
        folder.mkdirs()
    }

    def function(def val) {
        if (val) {
            int id = val.key
            String method = val.value.name.replaceAll(" ", "")

            functions.put(id, [id: id, name: method, returnType: val.value?.returnType, isCommand: val.value.isCommand, requiresAck: val.value.requiresAck, arrayType: val.value?.arrayType, parameters:val.value?.parameters?.collectEntries { k, v ->
                [(k): ([name: v.name, typeId: v.typeId, typeName: lookupType(v.typeName).capitalize(), encoder: { it -> new GStringTemplateEngine().createTemplate(serailizedLookup.getOrDefault(v.typeName, '${it.value.name}.Pack(data, SendComplete)')).make([it: it]).toString() }, decoder: { it -> new GStringTemplateEngine().createTemplate(deserailizedLookup.getOrDefault(v.typeName, '${it.value.name}.UnPack(data)')).make([it: it]).toString() }])]
            }])
        } else {
            println(functions)
            // do write
            header.toFile().withWriter { w ->
                w << functionHeaderTemplate.make(
                        [
                                functions: functions
                        ]
                ).toString()
            }
            definition.toFile().withWriter { w ->
                w << functionImplementationTemplate.make(
                        [
                                functions: functions
                        ]
                ).toString()
            }
            headerConsole.toFile().withWriter { w ->
                w << functionConsoleHeaderTemplate.make(
                        [
                                functions: functions
                        ]
                ).toString()
            }
            definitionConsole.toFile().withWriter { w ->
                w << functionConsoleImplementationTemplate.make(
                        [
                                functions: functions
                        ]
                ).toString()
            }
        }
        println(val)
    }

    def lookupType(def type) {
        typeLookupMap.getOrDefault(type, type)
    }

    def fieldConversion(def fields) {
        fields.collectEntries { k, v ->
            [k, [id: k, type: lookupType(v.class), name: v.name.capitalize(), encoder: { it -> new GStringTemplateEngine().createTemplate(serailizedLookup.getOrDefault(v.class, '${it.value.name}.Serialize(data, SendComplete)')).make([it: it]).toString() }]]
        }
    }

    static String toCamelCase(String text, boolean capitalized = false) {
        text = text.replaceAll("(_)([A-Za-z0-9])", { Object[] it -> it[2].toUpperCase() })
        return capitalized ? capitalize(text) : text
    }

    static String toSnakeCase(String text) {
        text.replaceAll(/([A-Z])/, /_$1/).toLowerCase().replaceAll(/^_/, '')
    }
}