import groovy.transform.SourceURI
import java.nio.file.Path
import java.nio.file.Paths
import groovy.text.GStringTemplateEngine

import static Shared.*

class CppTypeGen {

    @SourceURI
    URI sourceUri
    Path scriptLocation = Paths.get(sourceUri).getParent()

    GStringTemplateEngine engine = new GStringTemplateEngine()
    def typeHeaderTemplate = engine.createTemplate(scriptLocation.resolve("type-header.templ").toFile().text)
    def typeImplementationTemplate = engine.createTemplate(scriptLocation.resolve("type-implementation.templ").toFile().text)
    def lookUpHeaderTemplate = engine.createTemplate(scriptLocation.resolve("lookup-header.templ").toFile().text)
    def lookUpImplementationTemplate = engine.createTemplate(scriptLocation.resolve("lookup-implementation.templ").toFile().text)

    def folder = new File("generated/c++/packets")
    String removePath = "com.immortalember.relay.data"

    Map<Integer, String> TypeLookUp = [:]

    CppTypeGen() {
        folder.mkdirs()
    }

    def type(def val) {
        if (!val) {
            Path header = Paths.get(folder.absolutePath, "..", "NetworkLookUpTable.h")
            Path definition = Paths.get(folder.absolutePath, "..", "NetworkLookUpTable.cpp")

            header.toFile().withWriter { w ->
                w << lookUpHeaderTemplate.make(
                        [
                                types: TypeLookUp
                        ]
                ).toString()
            }
            definition.toFile().withWriter { w ->
                w << lookUpImplementationTemplate.make(
                        [
                                types: TypeLookUp
                        ]
                ).toString()
            }
        } else if (val.key >= 10) {
            TypeLookUp.put(val.key, val.value.name)

            Path header = Paths.get(folder.absolutePath, val.value.path.replaceFirst(removePath, ""), val.value.class + ".h")
            Path definition = Paths.get(folder.absolutePath, val.value.path.replaceFirst(removePath, ""), val.value.class + ".cpp")
            header.toFile().withWriter { w ->
                w << typeHeaderTemplate.make(
                        [
                                typeid: val.key,
                                name  : val.value.name,
                                parent: val.value.parentName,
                                fields: fieldConversion(val.value.fields)
                        ]
                ).toString()
            }
            definition.toFile().withWriter { w ->
                w << typeImplementationTemplate.make(
                        [
                                typeid: val.key,
                                name  : val.value.name,
                                fields: fieldConversion(val.value.fields)
                        ]
                ).toString()
            }
//        println(folder.absolutePath)
//        println(val)
//        println(val.key)
//        println(val.value)
//        println(scriptLocation)
        }
    }
    def static fieldConversion(def fields) {
        fields.collectEntries { k, v ->
            [k, [id: k, isInherited: v.isInherited, type: lookupType(v.class), name: v.name.capitalize(), encoder: { it -> new GStringTemplateEngine().createTemplate(serailizedLookup.getOrDefault(v.class, '${it.value.name}.Pack(data, SendComplete)')).make([it: it]).toString() }, decoder: { it -> new GStringTemplateEngine().createTemplate(deserailizedLookup.getOrDefault(v.class, '${it.value.name}.UnPack(data)')).make([it: it]).toString() }]]
        }
    }

    static String toCamelCase(String text, boolean capitalized = false) {
        text = text.replaceAll("(_)([A-Za-z0-9])", { Object[] it -> it[2].toUpperCase() })
        return capitalized ? capitalize(text) : text
    }

    static String toSnakeCase(String text) {
        text.replaceAll(/([A-Z])/, /_$1/).toLowerCase().replaceAll(/^_/, '')
    }
}