package com.immortalember.tests

import com.immortalember.relay.Statics
import com.immortalember.relay.data.ServerTransform
import com.immortalember.relay.util.Octree
import com.immortalember.relay.util.WeakOctree
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("Octree")
@DisplayName("Ocrtee")
class OctreeTests {
    static int maxSize

    @BeforeAll
    static void initialize() {
        maxSize = Statics.CONFIG.getInt("octree size")
    }

    @Test
    void tooSmall() {
        Octree<ServerTransform> tree = new Octree<>()
        ServerTransform ent = new ServerTransform()
        tree.insert(ent)
        Assertions.assertEquals([], tree.searchInBox(-10, -10, 10, 10, 10, -10))
    }

    @Test
    void tooLarge() {
        Octree<ServerTransform> tree = new Octree<>()
        ServerTransform ent = new ServerTransform()
        tree.insert(ent)
        Assertions.assertEquals(1, tree.searchInBox(-1000, -1000, 1000, 1000, 1000, -1000).size())
    }

    @Test
    void tooSmallSameLocation() {
        Octree<ServerTransform> tree = new Octree<>()
        (0..maxSize).each {
            ServerTransform ent = new ServerTransform()
            tree.insert(ent)
        }
        Assertions.assertEquals([], tree.searchInBox(-10, -10, 10, 10, 10, -10))
        Assertions.assertTrue(!tree.subdiveded)
    }

    @Test
    void tooLargeSameLocation() {
        Octree<ServerTransform> tree = new Octree<>()
        (0..maxSize).each {
            ServerTransform ent = new ServerTransform()
            tree.insert(ent)
        }
        Assertions.assertEquals(maxSize + 1, tree.searchInBox(-1000, -1000, 1000, 1000, 1000, -1000).size())
        Assertions.assertTrue(!tree.subdiveded)
    }

    @Test
    void tooSmallSubdivided() {
        Octree<ServerTransform> tree = new Octree<>()
        while (!tree.subdiveded) {
            ServerTransform ent = new ServerTransform()
            ent.positionX = 11 + Math.random() * 100
            ent.positionZ = 11 + Math.random() * 100
            ent.positionY = 11 + Math.random() * 100
            tree.insert(ent)
        }
        Assertions.assertEquals([], tree.searchInBox(-10, -10, 10, 10, 10, -10))
        Assertions.assertTrue(tree.subdiveded)
    }

    @Test
    void tooLargeSubdivided() {
        Octree<ServerTransform> tree = new Octree<>()
        int counter = 0
        while (!tree.subdiveded) {
            counter++
            ServerTransform ent = new ServerTransform()
            ent.positionX = (Math.random() - 0.5) * 1000
            ent.positionZ = (Math.random() - 0.5) * 1000
            ent.positionY = (Math.random() - 0.5) * 1000
            tree.insert(ent)
        }
        Assertions.assertEquals(counter, tree.searchInBox(-1000, -1000, 1000, 1000, 1000, -1000).size())
        Assertions.assertTrue(tree.subdiveded)
    }

    @Test
    void tooLargeSubdividedFiltered() {
        Octree<ServerTransform> tree = new Octree<>()
        int counter = 0
        while (!tree.subdiveded) {
            ServerTransform ent = new ServerTransform()
            ent.positionX = (Math.random() - 0.5) * 1000
            ent.positionZ = (Math.random() - 0.5) * 1000
            ent.positionY = (Math.random() - 0.5) * 1000
            if (Math.random() < 0.5) {
                counter++
                ent.data = [UUID.randomUUID()]
            }
            tree.insert(ent)
        }
        Assertions.assertEquals(counter, tree.searchInBox(-1000, -1000, 1000, 1000, 1000, -1000, {it.data.length != 0}).size())
        Assertions.assertTrue(tree.subdiveded)
    }

    @Test
    void containsCheck() {
        Octree<ServerTransform> tree = new Octree<>()
        ServerTransform ent = new ServerTransform()
        ent.positionX = 100
        ent.positionZ = 100
        ent.positionY = 100
        tree.insert(ent)
        ent = new ServerTransform()
        ent.positionX = -100
        ent.positionZ = -100
        ent.positionY = -100
        tree.insert(ent)
        Assertions.assertTrue(tree.pointIsInside(0, 0, 0))
    }

    @Test
    void partialCheck() {
        Octree<ServerTransform> tree = new Octree<>()
        ServerTransform ent = new ServerTransform()
        ent.positionX = 100
        ent.positionZ = 100
        ent.positionY = 100
        tree.insert(ent)
        ent = new ServerTransform()
        ent.positionX = -100
        ent.positionZ = -100
        ent.positionY = -100
        tree.insert(ent)
        ent = new ServerTransform()
        ent.positionX = -200
        ent.positionZ = -120
        ent.positionY = -200
        tree.insert(ent)
        Assertions.assertEquals(3, tree.children.size())
        Assertions.assertEquals(1, tree.searchInBox(0, 300, 300, 0, 300, 0).size())
    }

    @Test
    void weakTest() {
        Octree<ServerTransform> tree = new WeakOctree<>()
        ServerTransform ent = new ServerTransform()
        tree.insert(ent)
        Assertions.assertEquals(1, tree.searchInBox(-1000, -1000, 1000, 1000, 1000, -1000).size())
        ent = null
        System.gc()
        Assertions.assertEquals(0, tree.searchInBox(-1000, -1000, 1000, 1000, 1000, -1000).size())
    }
}
