package com.immortalember.relay.db

import com.immortalember.relay.Statics
import org.neo4j.driver.v1.Session
import org.neo4j.driver.v1.Transaction
import org.neo4j.driver.v1.StatementResult;

class SearchFunctions {
    public static UUID[] findUserCharacters(UUID id) {
        List<UUID> ids = new ArrayList<>()
        try (Session session = Statics.DB_CONNECTION.session(); Transaction tx = session.beginTransaction()) {
            StatementResult result = tx.run('MATCH(u:User { id: $id })-[:Owner]-(c:PlayerCharacter {deleted: false}) RETURN c.id AS character', [id:id.toString()])
            while(result.hasNext()) {
                ids.add(UUID.fromString(result.next().get(0).asString()))
            }
            tx.success()
        }

        UUID[] results = new UUID[ids.size()]
        ids.toArray(results)
        results
    }
}
