package com.immortalember.relay.db

import com.immortalember.relay.Statics
import com.immortalember.relay.Util
import groovy.util.logging.Slf4j
import org.neo4j.driver.v1.Record
import org.neo4j.driver.v1.Session
import org.neo4j.driver.v1.StatementResult
import org.neo4j.driver.v1.Transaction

import java.lang.reflect.Field
import java.lang.reflect.Modifier

@Slf4j
abstract class DBData implements IServerIdentifiable {
    protected int version = 0
    @DBField
    protected boolean deleted = false
    @DBField
    protected long deletedAt

    static void save(DBData value) {
        if (value == null) return
        value.version++
        try (Session session = Statics.DB_CONNECTION.session(); Transaction tx = session.beginTransaction()) {
            List<Tuple<String>> relations = new ArrayList<>()
            Map<String, Object> data = new HashMap<>()
            Util.getDBFields(value.getClass()).each {
                DBField annotation = it.getAnnotations().find {
                    it.annotationType() == DBField
                }

                if (annotation != null && (annotation.value() != Object || !annotation.dbType().isEmpty())) {
                    String type = annotation.value() == Object ? annotation.dbType() : annotation.value().simpleName
                    if (it.getType() == UUID) {
                        relations.add(new Tuple<>(type, value[it.name].toString(), !annotation.relationShipName().isEmpty() ? annotation.relationShipName() : "${value.getClass().simpleName}__${type}"))
                    } else {
                        throw new Exception("Relationship to complex types not yet implemented")
                    }
                } else {
                    if (it.getType() == UUID) {
                        data.put(it.name, value[it.name].toString())
                    } else {
                        data.put(it.name, value[it.name])
                    }
                }
            }
            data.put("version", value.version)

            if (value.version == 1) {
                StringBuffer str = 'CREATE (n:' << value.getClass().simpleName << ' {'
                data.keySet().eachWithIndex { k, i ->
                    str = str << k << ': $' << k
                    if (i < data.size() - 1) {
                        str = str << ", "
                    }
                }
                str = str << '}) RETURN n'
                tx.run(str.toString(), data)

                String start
                StringBuffer match = 'MATCH (n:' << value.getClass().simpleName << ' {'
                data.keySet().eachWithIndex { k, i ->
                    match = match << k << ': $' << k
                    if (i < data.size() - 1) {
                        match = match << ", "
                    }
                }
                match = match << '})'
                start = match.toString()
                relations.each {
                    tx.run("$start MATCH (o${!it[0].isEmpty() ? ':' + it[0] : ''} {id: '${it[1]}'}) CREATE (n)<-[:${it[2]}]-(o)", data)
                }
            } else {
                StringBuffer str = 'MATCH (n:' << value.getClass().simpleName << ' { id: \$id}) SET n = \$props RETURN n'
                tx.run(str.toString(), [id: data.id, props: data])

                String start
                StringBuffer match = 'MATCH (n:' << value.getClass().simpleName << ' { id: \$id})'
                start = match.toString()
                relations.each {
                    tx.run("$start MATCH (o${!it[0].isEmpty() ? ':' + it[0] : ''} {id: '${it[1]}'}) MERGE (n)<-[:${it[2]}]-(o)", data)
                }
            }
            tx.success()
        }
    }

    static <T extends DBData> T retrieve(Class<T> type, UUID id, List<String> fields = new ArrayList<>(), boolean includeDeleted = false) {
        T result = null
        try (Session session = Statics.DB_CONNECTION.session(); Transaction tx = session.beginTransaction()) {
            List<Tuple<String>> relations = new ArrayList<>()
            Util.getDBFields(type).each {
                DBField annotation = it.getAnnotations().find {
                    it.annotationType() == DBField
                }

                if (annotation != null && (annotation.value() != Object || !annotation.dbType().isEmpty())) {
                    String ftype = annotation.value() == Object ? annotation.dbType() : annotation.value().simpleName
                    if (it.getType() == UUID) {
                        relations.add(new Tuple<>(ftype, it.name, !annotation.relationShipName().isEmpty() ? annotation.relationShipName() : "${type.simpleName}__${ftype}"))
                    } else {
                        throw new Exception("Relationship to complex types not yet implemented")
                    }
                }
            }

            StatementResult data = tx.run("MATCH (o:${type.simpleName} {id:\$id}) RETURN o", [id: id.toString()])
            if (data.hasNext()) {
                Record next = data.next()
                if (!next.get(0).get("deleted", false)) {
                    result = type.newInstance(new Object[0])
                    result.setID(id)

                    next.get(0).asMap().each {
                        if (it.key != "id" && (fields.empty || fields.contains(it.key))) {
                            result[it.key] = it.getValue()
                        }
                    }

                    relations.each {
                        StatementResult relation = tx.run("MATCH (n:${it[0]})-[:${it[2]}]-(o:${type.simpleName} {id: \$id}) RETURN n.id AS id", [id: id.toString()])

                        if (relation.hasNext()) {
                            result[it[1]] = UUID.fromString(relation.next().get(0).asString())
                        }
                    }
                }
            }

            log.info("Found {} for {}", result, "MATCH (o:${type.simpleName} {id:$id}) RETURN o")
            tx.success()
        }

        result
    }

    static void delete(DBData data) {
        if(data != null) {
            data.deleted = true
            data.deletedAt = System.currentTimeMillis()
            save(data)
        }
    }

    static void purge(DBData data) {
        UUID id = data.getID()

        try (Session session = Statics.DB_CONNECTION.session(); Transaction tx = session.beginTransaction()) {
            tx.run("MATCH (n:${data.getClass().simpleName} {id:\$id}) DELETE n", [id: id.toString()])
        }
    }
}
