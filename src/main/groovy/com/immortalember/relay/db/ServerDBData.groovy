package com.immortalember.relay.db

import com.immortalember.relay.packets.ObjectPacket

abstract class ServerDBData extends DBData {
    @DBField
    protected UUID id

    @Override
    void setID(UUID id) { this.id = id };

    @Override
    UUID getID() { id };

    public static IServerIdentifiable create(IServerIdentifiable data, ObjectPacket packet) {
        save(data as DBData)
        data
    }
}
