package com.immortalember.relay.db

interface IServerIdentifiable {
    void setID(UUID id);

    UUID getID();
}