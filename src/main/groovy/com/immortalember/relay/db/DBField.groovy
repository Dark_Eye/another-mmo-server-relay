package com.immortalember.relay.db

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy.RUNTIME)
@Target([ElementType.FIELD])
@interface DBField {
    Class<? extends ServerDBData> value() default Object.class
    String dbType() default ''
    String relationShipName() default ''
    String otherField() default 'id'
}