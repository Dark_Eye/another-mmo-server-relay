package com.immortalember.relay

import groovy.util.logging.Slf4j
import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.socket.DatagramPacket

@Slf4j
class UserDatapacket extends SimpleChannelInboundHandler<DatagramPacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
        messageReceived ctx, msg
    }

    protected loopThrough(def data, Closure done, int id = 0) {
        Statics.INCOMING_DATA_PRE_PROCESSORS.get(id).sendAndContinue(data, {
            int newId = id + 1

            if (Statics.INCOMING_DATA_PRE_PROCESSORS.size() > newId) {
                loopThrough(it, done, newId)
            } else {
                done.call(it)
            }
        })
    }

    protected void messageReceived(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
        ByteBuf buf = msg.content()
        final byte[] data = new byte[buf.readableBytes()]
        buf.readBytes(data)

        loopThrough([sender: msg.sender(), data: data], Statics.server::addPacket)

//        preProcessorUser.send([sender: msg.sender(), data: data])
    }
}
