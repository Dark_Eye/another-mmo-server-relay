package com.immortalember.relay.util

import java.util.concurrent.ConcurrentHashMap

class BiMap<K, V> implements Map<K, V> {
    Map lr, rl

    BiMap(Map<K, V> lr, Map<V, K> rl) {
        this.lr = lr
        this.rl = rl
    }

    BiMap() {
        lr = new ConcurrentHashMap<>()
        rl = new ConcurrentHashMap<>()
    }

    @Override
    int size() {
        return rl.size()
    }

    @Override
    boolean isEmpty() {
        return rl.isEmpty()
    }

    @Override
    boolean containsKey(Object key) {
        return rl.containsKey(key)
    }

    @Override
    boolean containsValue(Object value) {
        return lr.containsKey(value)
    }

    @Override
    V get(Object key) {
        return rl.get(key)
    }

    K getKey(Object value) {
        return lr.get(value)
    }

    @Override
    V put(K key, V value) {
        rl.put(key, value)
        lr.put(value, key)
        return value
    }

    @Override
    V remove(Object key) {
        V val = rl.remove(key)
        lr.remove(val)
        return val
    }

    @Override
    void putAll(Map<? extends K, ? extends V> m) {
        m.each {k, v -> put(k, v)}
    }

    @Override
    void clear() {
        rl.clear()
        lr.clear()
    }

    @Override
    Set<K> keySet() {
        return rl.keySet()
    }

    @Override
    Collection<V> values() {
        return lr.keySet()
    }

    @Override
    Set<Entry<K,V>> entrySet() {
        return rl.entrySet()
    }
}
