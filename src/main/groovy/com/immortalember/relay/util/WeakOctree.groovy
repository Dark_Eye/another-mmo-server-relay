package com.immortalember.relay.util


import com.immortalember.relay.data.ServerTransform
import groovy.util.logging.Slf4j

import java.lang.ref.WeakReference

@Slf4j
class WeakOctree<T extends ServerTransform> extends Octree<T> {

    boolean outer = true, subdived = false
    Map<Tuple<Float>, List<WeakReference<T>>> children = [:]


    public WeakOctree() {}

    public WeakOctree(T... items) {
        items.each {
            insert(it)
        }
    }

    protected WeakOctree(WeakOctree<T> parent, int north, int west, int south, int east, int top, int bottom) {
        outer = false
        this.parent = parent
        this.north = north
        this.west = west
        this.south = south
        this.east = east
        this.top = top
        this.bottom = bottom
    }

    @Override
    public void insert(T obj) {
        if (outer) {
            resize(Math.min(north, (int) obj.positionY), Math.max(west, (int) obj.positionX), Math.max(south, (int) obj.positionY), Math.min(east, (int) obj.positionX), Math.max(top, (int) obj.positionZ), Math.min(bottom, (int) obj.positionZ))
        }

        if (!subdived) {
            List<T> chilens = children.getOrDefault(new Tuple<Float>(obj.positionX, obj.positionY, obj.positionZ), new ArrayList<>())
            chilens.add(new WeakReference<>(obj))
            children.put(new Tuple<Float>(obj.positionX, obj.positionY, obj.positionZ), chilens)
            if (children.size() > maxSize) {
                topNorthEast = new WeakOctree<T>(parent, north, (int) ((west + east) / 2), (int) ((north + south) / 2), east, top, (int) ((top + bottom) / 2))
                topNorthWest = new WeakOctree<T>(parent, north, west, (int) ((north + south) / 2), (int) ((west + east) / 2), top, (int) ((top + bottom) / 2))
                topSouthEast = new WeakOctree<T>(parent, (int) ((north + south) / 2), (int) ((west + east) / 2), south, east, top, (int) ((top + bottom) / 2))
                topSouthWest = new WeakOctree<T>(parent, (int) ((north + south) / 2), west, south, (int) ((west + east) / 2), top, (int) ((top + bottom) / 2))

                bottomNorthEast = new WeakOctree<T>(parent, north, (int) ((west + east) / 2), (int) ((north + south) / 2), east, (int) ((top + bottom) / 2), bottom)
                bottomNorthWest = new WeakOctree<T>(parent, north, west, (int) ((north + south) / 2), (int) ((west + east) / 2), (int) ((top + bottom) / 2), bottom)
                bottomSouthEast = new WeakOctree<T>(parent, (int) ((north + south) / 2), (int) ((west + east) / 2), south, east, (int) ((top + bottom) / 2), bottom)
                bottomSouthWest = new WeakOctree<T>(parent, (int) ((north + south) / 2), west, south, (int) ((west + east) / 2), (int) ((top + bottom) / 2), bottom)

                children.removeAll { k, v ->
                    v.removeAll {
                        T chk = it.get()
                        if (chk != null && (filter == null || filter(chk))) {
                            subInsert(chk)
                        }
                    }
                    v.isEmpty()
                }
                subdived = true
                children = [:]
            }
        } else {
            subInsert(obj)
        }
    }

    public List<T> searchInBox(int north, int west, int south, int east, int top, int bottom, Closure<Boolean> filter = null) {
        List<T> result = new ArrayList<>()
        if (north > south) {
            int swap = south
            south = north
            north = swap
        }
        if (east > west) {
            int swap = east
            east = west
            west = swap
        }
        if (bottom > top) {
            int swap = top
            top = bottom
            bottom = swap
        }
        if (south < this.north || north > this.south || west < this.east || east > this.west || bottom > this.top || top < this.bottom) {
        } else if (subdived) {
            result.addAll(topNorthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(topNorthWest.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(topSouthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(topSouthWest.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomNorthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomNorthWest.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomSouthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomSouthWest.searchInBox(north, west, south, east, top, bottom, filter))
        } else if (north <= this.north && south >= this.south && east >= this.east && west <= this.west && top >= this.top && bottom <= this.bottom) {
            children.removeAll { k, v ->
                v.removeAll {
                    T chk = it.get()
                    if (chk != null && (filter == null || filter(chk))) {
                        result.add(chk)
                    }
                    chk == null
                }
                v.isEmpty()
            }
        } else {
            children.removeAll { k, v ->
                v.removeAll {
                    T chk = it.get()
                    if (chk != null && inPointCheck(north, west, south, east, top, bottom, chk.positionX, chk.positionY, chk.positionZ) && (filter == null || filter(chk))) {
                        result.add(chk)
                    }
                    chk == null
                }
                v.isEmpty()
            }
        }

        return result
    }

    @Override
    void purge(T obj) {
        children.removeAll { k, v ->
            v.removeAll( {
                it.get() == obj || it.get() == null
            })
            v.isEmpty()
        }
    }

}