package com.immortalember.relay.util

import com.immortalember.relay.Statics
import com.immortalember.relay.data.ServerTransform
import groovy.util.logging.Slf4j

@Slf4j
class Octree<T extends ServerTransform> {

    boolean outer = true, subdiveded = false
    Map<Tuple<Float>, List<T>> children = [:]
    Map<T, Octree<T>> locationStorage = new WeakHashMap<>()

    int maxSize = Statics.CONFIG.getInt("octree size")
    int north, west, south, east, top, bottom

    Octree<T> topNorthEast, topNorthWest, topSouthEast, topSouthWest,
              bottomNorthEast, bottomNorthWest, bottomSouthEast, bottomSouthWest
    Octree<T> parent

    public Octree() {}

    public Ocrtree(T... items) {
        items.each {
            insert(it)
        }
    }

    protected Octree(Octree<T> parent, int north, int west, int south, int east, int top, int bottom) {
        outer = false
        this.parent = parent
        this.north = north
        this.west = west
        this.south = south
        this.east = east
        this.top = top
        this.bottom = bottom
    }

    protected void resize(int north, int west, int south, int east, int top, int bottom) {
        boolean resized = false
        if (north < this.north) {
            resized = true
            this.north = north
        }
        if (west > this.west) {
            resized = true
            this.west = west
        }
        if (south > this.south) {
            resized = true
            this.south = south
        }
        if (east < this.east) {
            resized = true
            this.east = east
        }
        if (top > this.top) {
            resized = true
            this.top = top
        }
        if (bottom < this.bottom) {
            resized = true
            this.bottom = bottom
        }
        if (resized && subdiveded) {
            topNorthEast.resize(north, topNorthEast.west, topNorthEast.south, east, top, topNorthEast.bottom)
            topNorthWest.resize(north, west, topNorthWest.south, topNorthWest.east, top, topNorthWest.bottom)
            topSouthEast.resize(topSouthEast.north, topSouthEast.west, south, east, top, topSouthEast.bottom)
            topSouthWest.resize(topSouthWest.north, west, south, topSouthWest.east, top, topSouthWest.bottom)

            bottomNorthEast.resize(north, bottomNorthEast.west, bottomNorthEast.south, east, bottomNorthEast.top, bottom)
            bottomNorthWest.resize(north, west, bottomNorthWest.south, bottomNorthWest.east, bottomNorthEast.top, bottom)
            bottomSouthEast.resize(bottomSouthEast.north, bottomSouthEast.west, south, east, bottomNorthEast.top, bottom)
            bottomSouthWest.resize(bottomSouthWest.north, west, south, bottomSouthWest.east, bottomNorthEast.top, bottom)
        }
    }

    public void insert(T obj) {
        if (locationStorage.containsKey(obj)) {
            update(obj)
            return
        }
        if (outer) {
            resize(Math.min(north, (int) obj.positionY), Math.max(west, (int) obj.positionX), Math.max(south, (int) obj.positionY), Math.min(east, (int) obj.positionX), Math.max(top, (int) obj.positionZ), Math.min(bottom, (int) obj.positionZ))
        }

        if (subdiveded) {
            subInsert(obj)
        } else {
            List<T> chilens = children.getOrDefault(new Tuple<Float>(obj.positionX, obj.positionY, obj.positionZ), new ArrayList<>())
            chilens.add(obj)
            children.put(new Tuple<Float>(obj.positionX, obj.positionY, obj.positionZ), chilens)
            getRootOctree().locationStorage.put(obj, this)
            if (children.size() > maxSize) {
                subdived()
            }
        }
    }

    void subInsert(T it) {
        if ((top + bottom) / 2 < it.positionZ) { // top
            if ((north + south) / 2 < it.positionY) { //south
                if ((west + east) / 2 < it.positionX) { //west
                    topSouthWest.insert(it)
                } else {
                    topSouthEast.insert(it)
                }
            } else {
                if ((west + east) / 2 < it.positionX) { //west
                    topNorthWest.insert(it)
                } else {
                    topNorthEast.insert(it)
                }
            }
        } else {
            if ((north + south) / 2 < it.positionY) { //south
                if ((west + east) / 2 < it.positionX) { //west
                    bottomSouthWest.insert(it)
                } else {
                    bottomSouthEast.insert(it)
                }
            } else {
                if ((west + east) / 2 < it.positionX) { //west
                    bottomNorthWest.insert(it)
                } else {
                    bottomNorthEast.insert(it)
                }
            }
        }
    }

    void subdived() {
        if (subdiveded) return
        topNorthEast = new Octree<T>(parent, north, (int) ((west + east) / 2), (int) ((north + south) / 2), east, top, (int) ((top + bottom) / 2))
        topNorthWest = new Octree<T>(parent, north, west, (int) ((north + south) / 2), (int) ((west + east) / 2), top, (int) ((top + bottom) / 2))
        topSouthEast = new Octree<T>(parent, (int) ((north + south) / 2), (int) ((west + east) / 2), south, east, top, (int) ((top + bottom) / 2))
        topSouthWest = new Octree<T>(parent, (int) ((north + south) / 2), west, south, (int) ((west + east) / 2), top, (int) ((top + bottom) / 2))

        bottomNorthEast = new Octree<T>(parent, north, (int) ((west + east) / 2), (int) ((north + south) / 2), east, (int) ((top + bottom) / 2), bottom)
        bottomNorthWest = new Octree<T>(parent, north, west, (int) ((north + south) / 2), (int) ((west + east) / 2), (int) ((top + bottom) / 2), bottom)
        bottomSouthEast = new Octree<T>(parent, (int) ((north + south) / 2), (int) ((west + east) / 2), south, east, (int) ((top + bottom) / 2), bottom)
        bottomSouthWest = new Octree<T>(parent, (int) ((north + south) / 2), west, south, (int) ((west + east) / 2), (int) ((top + bottom) / 2), bottom)

        children.each { k, v ->
            v.each {
                subInsert(it)
            }
        }
        subdiveded = true
        children = [:]
    }

    public List<T> searchInBox(int north, int west, int south, int east, int top, int bottom, Closure<Boolean> filter = null) {
        List<T> result = new ArrayList<>()
        if (north > south) {
            int swap = south
            south = north
            north = swap
        }
        if (east > west) {
            int swap = east
            east = west
            west = swap
        }
        if (bottom > top) {
            int swap = top
            top = bottom
            bottom = swap
        }
        if (south < this.north || north > this.south || west < this.east || east > this.west || bottom > this.top || top < this.bottom) {
        } else if (subdiveded) {
            result.addAll(topNorthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(topNorthWest.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(topSouthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(topSouthWest.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomNorthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomNorthWest.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomSouthEast.searchInBox(north, west, south, east, top, bottom, filter))
            result.addAll(bottomSouthWest.searchInBox(north, west, south, east, top, bottom, filter))
        } else if (north <= this.north && south >= this.south && east >= this.east && west <= this.west && top >= this.top && bottom <= this.bottom) {
            if (filter == null) {
                children.each { k, v ->
                    result.addAll(v)
                }
            } else {
                children.each({ k, v ->
                    result.addAll(v.findAll(filter))
                })
            }
        } else {
            children.each({ k, v ->
                result.addAll(v.findAll({
                    inPointCheck(north, west, south, east, top, bottom, it.positionX, it.positionY, it.positionZ) && (filter == null || filter(it))
//                    north >= it.positionY && south <= it.positionY && east >= it.positionX && west <= it.positionX && top <= it.positionZ && bottom >= it.positionZ && (filter == null || filter(it))
                }))
            })
        }


        return result
    }

    public List<T> searchInRadius(int x, int y, int z, float radius, Closure<Boolean> filter = null) {
        List<T> result = searchInBox((int)(y - radius), (int)(x - radius), (int)(y + radius), (int)(x + radius), (int)(z + radius), (int)(z - radius), filter)

        return result
    }

    protected static boolean inPointCheck(int north, int west, int south, int east, int top, int bottom, float positionX, float positionY, float positionZ) {
        north <= positionY && south >= positionY && east <= positionX && west >= positionX && top >= positionZ && bottom <= positionZ
    }

    public boolean pointIsInside(float positionX, float positionY, float positionZ) {
        inPointCheck(north, west, south, east, top, bottom, positionX, positionY, positionZ)
    }

    Octree<T> getRootOctree() {
        Octree<T> root = this
        while (root.parent != null) {
            root = root.parent
        }
        root
    }

    void update(T obj) {
        Octree<T> tree = locationStorage.get(obj)
        if (tree != null) {
            if (!tree.pointIsInside(obj.positionX, obj.positionY, obj.positionZ)) {
                tree.purge(obj)
                insert(obj)
            }
        }
    }

    void remove(T obj) {
        Octree<T> tree = locationStorage.get(obj)
        if (tree != null) {
            tree.purge(obj)
        }
    }

    void purge(T obj) {
        children.removeAll { k, v ->
            v.remove(obj)
            v.isEmpty()
        }
    }
}