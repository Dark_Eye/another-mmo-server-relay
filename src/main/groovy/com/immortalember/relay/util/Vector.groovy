package com.immortalember.relay.util

class Vector {
    float x, y, z

    Vector() {
    }

    Vector(float x, float y, float z) {
        this.x = x
        this.y = y
        this.z = z
    }

    void normalize() {
        float max = Math.max(Math.max(Math.abs(x), Math.abs(y)), Math.abs(z))

        x = max == 0 ? 0 : (x / max)
        y = max == 0 ? 0 : (y / max)
        z = max == 0 ? 0 : (z / max)
    }

    Vector crossProduct(Vector direction) {
        crossProduct(direction.x, direction.y, direction.z)
    }

    Vector crossProduct(float xD, float yD, float zD) {
        new Vector((float)(y * zD - z * yD), (float)(z * xD - x * zD), (float)(x * yD - y * xD))
    }
}
