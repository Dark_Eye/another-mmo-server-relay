package com.immortalember.relay.util

class Matrix4x4f {
    float[][] matrix = new float[4][4]

    Matrix4x4f() {
    }

    Matrix4x4f(Vector inX, Vector inY, Vector inZ, Vector inW) {
        matrix[0][0] = inX.x; matrix[0][1] = inX.y; matrix[0][2] = inX.z; matrix[0][3] = 0.0f; // x
        matrix[1][0] = inY.x; matrix[1][1] = inY.y; matrix[1][2] = inY.z; matrix[1][3] = 0.0f; // y
        matrix[2][0] = inZ.x; matrix[2][1] = inZ.y; matrix[2][2] = inZ.z; matrix[2][3] = 0.0f; // z
        matrix[3][0] = inW.x; matrix[3][1] = inW.y; matrix[3][2] = inW.z; matrix[3][3] = 1.0f;
    }


}
