package com.immortalember.relay.server

import com.immortalember.relay.Statics
import com.immortalember.relay.ai.AIHost
import com.immortalember.relay.data.ServerTransform
import com.immortalember.relay.data.PlayerCharacter
import com.immortalember.relay.db.DBData
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.packets.ObjectPacket
import com.immortalember.relay.packets.Packet
import com.immortalember.relay.util.BiMap
import com.immortalember.relay.util.Octree
import com.immortalember.relay.util.WeakOctree
import groovy.util.logging.Slf4j

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.ReentrantLock

@Slf4j
class Server implements Runnable {
    static final int PLAYER_RADIUS = Statics.CONFIG.getDouble("player radius")

    Map<UUID, Map> userDetailsLookup = new HashMap<>()
    Map<UUID, IServerIdentifiable> objectRegistry = new ConcurrentHashMap<>()

    Set<UUID> spawningPlayers = Collections.newSetFromMap(new ConcurrentHashMap<>())
    BiMap<UUID, UUID> userCharacterLookup = new BiMap<>()

    Map<ServerTransform, Map<Integer, Set<Integer>>> dirtyEntities = new ConcurrentHashMap<>()

    Octree<ServerTransform> entities = new WeakOctree<>()
    Octree<PlayerCharacter> players = new WeakOctree<>()

    List<Packet> inputBuffer = new LinkedList<>()

    private ReentrantLock packetLock = new ReentrantLock()
    private ReentrantLock entityLock = new ReentrantLock()

    AIHost aiHost = new AIHost(this)

    long lastTime = System.nanoTime()

    @Override
    void run() {
        while (Statics.isRunning) {
            long time = System.nanoTime()
            float detlaTime = (time - lastTime) / 1000000

            // TODO: take input processing
            LinkedList<Packet> input = swapPackets()
            input.sort({ l, r ->
                (l.time <=> r.time)
            })

            for (Packet p : input) {
                p.doCall()
                if (p.type == Packet.Type.OBJECT_UPDATE && p instanceof ObjectPacket) {
                    IServerIdentifiable obj = objectRegistry.get(p.objectId)
                    if (obj instanceof ServerTransform) {
                        entities.update(obj)
                        if (obj instanceof PlayerCharacter) {
                            players.update(obj)
                        }
                    }
                }
            }

            aiHost.doRun(detlaTime)

            swapDirtyEntities().each { k, v ->
                players.searchInRadius((int)k.positionX, (int)k.positionY, (int)k.positionZ, PLAYER_RADIUS).each { player ->
                    v.each { type, fields ->
                        if (player instanceof PlayerCharacter) {
                            Statics.outputBuffer.addPacket(new ObjectPacket(type, k, fields, player.player))
                        }
                    }
                }
            }

            spawningPlayers.each { id ->
                spawningPlayers.remove(id)
                PlayerCharacter player = (PlayerCharacter)objectRegistry.get(id)
                players.insert(player)
                entities.insert(player)
                entities.searchInRadius((int)player.positionX, (int)player.positionY, (int)player.positionZ, PLAYER_RADIUS).each { ent ->
                    if (ent.getID() != id) {
                        Statics.outputBuffer.addPacket(new ObjectPacket(Packet.Type.OBJECT_CREATE, ent, ent.FIELD_MAPPING.keySet(), player.player))
                    }
                }
            }

            // TODO: do world

            Statics.outputBuffer.dispatchPackets()

            lastTime = time

            long dif = Math.max(Statics.WAIT_TIME - System.nanoTime() - time as long, 0L)
            long mil = (long) (dif / 1000000)
            int nano = dif % 1000000
            //500000 nanos is rounded to milliseconds
            Thread.sleep(mil, nano)
        }
        log.info("Server is shutting down")
        store()
        log.info("Server safe to terminate")
    }

    public void store() {
        objectRegistry.each { k, v ->
            if (v instanceof DBData) {
                v.save(v)
            }
        }
    }

    void addPacket(Packet packet) {
        packetLock.lock()
        try {
            inputBuffer.add(packet)
        } finally {
            packetLock.unlock()
        }
    }

    List<Packet> swapPackets() {
        packetLock.lock()
        List<Packet> lag = inputBuffer
        try {
            inputBuffer = new LinkedList<>()
        } finally {
            packetLock.unlock()
        }
        return lag
    }

    void markEntityDirty(ServerTransform entity, int type, Collection<Integer> fields) {
        if (fields.isEmpty()) return

        entityLock.lock()

        try {
            Map<Integer, Set<Integer>> marks = dirtyEntities.getOrDefault(entity, [:])
            Set<Integer> fieldz = marks.getOrDefault(type, new HashSet<>())

            fieldz.addAll(fields)
            marks.put(type, fieldz)
            dirtyEntities.put(entity, marks)
        } finally {
            entityLock.unlock()
        }
    }

    Map<ServerTransform, Map<Integer, Set<Integer>>> swapDirtyEntities() {
        entityLock.lock()
        Map<ServerTransform, Map<Integer, Set<Integer>>> res

        try {
            res = dirtyEntities
            dirtyEntities = new HashMap<>()
        } finally {
            entityLock.unlock()
        }

        return res
    }

    public UUID registerObject(IServerIdentifiable ob) {
        UUID id
        do {
            id = UUID.randomUUID()
        } while (objectRegistry.containsKey(id))

        objectRegistry.put(id, ob)
        ob.setID(id)

        if (ob instanceof ServerTransform && !(ob instanceof PlayerCharacter)) {
            entities.insert(ob)
//            if (ob instanceof PlayerCharacter) {
//                players.insert(ob)
//            }
        }

        id
    }

    public <T extends IServerIdentifiable> T retrieveObject(Class<T> type, UUID id) {
        if (objectRegistry.containsKey(id)) {
            (T) objectRegistry.get(id)
        } else {
            (T) reAddObject((T)DBData.retrieve(type, id))
        }
    }

    protected <T extends IServerIdentifiable> T reAddObject(T obj) {
        if (obj == null) return

        objectRegistry.put(obj.getID(), obj)

        if (obj instanceof ServerTransform && !(obj instanceof PlayerCharacter)) {
            entities.insert(obj)
        }

        obj
    }

}
