package com.immortalember.relay.packets

import com.immortalember.relay.Statics
import com.immortalember.relay.Util
import com.immortalember.relay.networking.functions.FunctionWrapper
import groovy.util.logging.Slf4j
import groovyx.gpars.dataflow.Promise

import java.nio.ByteBuffer

@Slf4j
class FunctionPacket extends Packet {
    long functionId


    FunctionPacket(Packet parent) {
        super(parent)

        ByteBuffer buffer = ByteBuffer.wrap(parent.data)
        functionId = Util.parseIntegral(buffer)
        data = new byte[buffer.remaining()]
        buffer.get(data)
    }

    @Override
    void doCall(Object... args) {
        List<Object> params = new ArrayList<>()
        params.add(this)
        params.addAll(args)
        FunctionWrapper um = Statics.functionMapping.get(functionId)
        def ret = um.function(params.toArray() as Object[])

        if (um.needsAck) {
            if (ret instanceof Promise) {
                ret.then {
                    if (!(it instanceof AckPacket)) {
                        sendAck(it)
                    }
                }
            } else {
                if (!(ret instanceof AckPacket)) {
                    sendAck(ret)
                }
            }
        }
    }

    @Override
    String toString() {
        return "FunctionPacket{" +
                "sender=" + sender +
                ", version=" + version +
                ", time=" + time +
                ", id=" + id +
                ", user=" + user +
                ", type=" + Integer.toBinaryString(type) +
                ", data=" + Arrays.toString(data as byte[]) +
                ", functionId=" + functionId +
                '}'
    }
}
