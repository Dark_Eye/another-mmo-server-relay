package com.immortalember.relay.packets

import com.immortalember.relay.Statics
import com.immortalember.relay.Util
import com.immortalember.relay.data.ServerTransform
import com.immortalember.relay.db.DBData
import com.immortalember.relay.db.IServerIdentifiable
import groovy.util.logging.Slf4j

import java.nio.ByteBuffer

@Slf4j
class ObjectPacket extends Packet {
    protected long typeId
    protected UUID objectId

    public static UUID INVALID_OBJECT_ID = new UUID(0, 0)

    ObjectPacket(Packet parent) {
        super(parent)

        ByteBuffer buffer = ByteBuffer.wrap(parent.data)
        objectId = new UUID(buffer.getLong(), buffer.getLong())
        typeId = Util.parseIntegral(buffer)
        data = new byte[buffer.remaining()]
        buffer.get(data)
    }

    ObjectPacket(int type, IServerIdentifiable object, Collection<Integer> fields, UUID user) {
        this.type = type
        typeId = Statics.classMapping.get(object.getClass())
        objectId = object.getID()

        try (ByteArrayOutputStream byts = new ByteArrayOutputStream()) {
            Util.writeValue(byts, objectId, false)
            Util.writeValue(byts, typeId, false)
            Util.writeObject(byts, object, false, fields)
            data = byts.toByteArray()
        }

        this.user = Statics.server.userDetailsLookup.get(user).user
        this.sender = Statics.server.userDetailsLookup.get(user).sender
    }

    @Override
    void doCall(Object... args) {
//        log.info("We are {} and got {}", this, args)
        def result
        switch (this.type) {
            case Type.OBJECT_CREATE:
                Set<Integer> keys = new HashSet<>()
                result = Statics.typeMapping.get(typeId).create(this, keys)
                if (result instanceof ServerTransform) {
                    Statics.server.markEntityDirty(result, Type.OBJECT_CREATE, keys)
                }
                break
            case Type.OBJECT_READ:
                result = Statics.server.retrieveObject(Statics.typeMapping.get(typeId).id, objectId)
                break
            case Type.OBJECT_UPDATE:
                result = Statics.server.retrieveObject(Statics.typeMapping.get(typeId).id, objectId)
                Set<Integer> keys = new HashSet<>()
                Util.readType(result, ByteBuffer.wrap(data), keys)
                if (result instanceof ServerTransform) {
                    Statics.server.markEntityDirty(result, Type.OBJECT_UPDATE, keys)
                }
                result = null
                break
            case Type.OBJECT_DELETE:
                DBData.delete(Statics.server.retrieveObject(Statics.typeMapping.get(typeId).id, objectId) as DBData)
                result = objectId
        }
//        log.info("and saying {}", result)
        sendAck(result)
    }

    @Override
    String toString() {
        return "ObjectPacket{" +
                "sender=" + sender +
                ", version=" + version +
                ", time=" + time +
                ", id=" + id +
                ", user=" + user +
                ", type=" + Integer.toBinaryString(type) +
                ", data=" + Arrays.toString(data as byte[]) +
                ", data=" + (data as byte[]).encodeHex() +
                ", typeId=" + typeId +
                ", objectId=" + objectId +
                '}'
    }
}
