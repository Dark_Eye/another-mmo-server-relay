package com.immortalember.relay.packets

class AckPacket extends Packet {
    AckPacket() {
        type = Type.ACK
    }

    AckPacket(byte[] data) {
        this()
        this.data = data
    }

    AckPacket(Packet packet) {
        super(packet)
        type = Type.ACK
    }

    AckPacket(Packet packet, byte[] data) {
        this(packet)
        this.data = data
    }

    @Override
    String toString() {
        return "AckPacket{" +
                "sender=" + sender +
                ", version=" + version +
                ", time=" + time +
                ", id=" + id +
                ", user=" + user +
                ", type=" + Integer.toBinaryString(type) +
                ", data=" + Arrays.toString(data as byte[]) +
                ", data=" + (data as byte[]).encodeHex() +
                '}'
    }
}
