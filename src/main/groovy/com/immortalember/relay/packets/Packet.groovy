package com.immortalember.relay.packets

import com.immortalember.relay.Statics
import com.immortalember.relay.Util
import groovy.util.logging.Slf4j

import java.nio.ByteBuffer

@Slf4j
class Packet {
    static class Type {
        static int OBJECT_CREATE = (0x00)
        static int OBJECT_READ = (0x10)
        static int OBJECT_UPDATE = (0x20)
        static int OBJECT_DELETE = (0x30)
        static int FUNCTION = (0x40)
        static int ACK = (0x80)
    }

    public static final Map<Integer, Class<Packet>> PACKET_TYPE_LOOKUP = [
            (Type.OBJECT_CREATE): ObjectPacket,
            (Type.OBJECT_READ)  : ObjectPacket,
            (Type.OBJECT_UPDATE): ObjectPacket,
            (Type.OBJECT_DELETE): ObjectPacket,
            (Type.FUNCTION)     : FunctionPacket,
            (Type.ACK)          : AckPacket
    ]

    InetSocketAddress sender
    int version = 1
    int id
    int type
    Map<String, String> user
    long time
    byte[] data = new byte[0], original

    Packet() {}
    Packet(Packet origin) {
        sender = origin.sender
        version = origin.version
        id = origin.id
        type = origin.type
        time = origin.time
        data = origin.data
        original = origin.original
        user = origin.user
    }

    protected AckPacket sendAck(Object data) {
        AckPacket result
        if (data instanceof AckPacket) {
            return
        } else {
            result = new AckPacket(this)
            if (data != null) {
                if (data instanceof Boolean) {
                    data = data ? 1 : 0
                }

                try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                    Util.writeValue(out, data)
                    result.data = out.toByteArray()
                }
            }
        }
        //log.info("ACKing with {}", data)

        Statics.outputBuffer.addPacket(result)
        result
    }

    byte[] serialize() {
        byte[] result
        long time = System.currentTimeMillis()
        id = id == 0 ? (int)(((int)(Math.random() * 8192)) - (8192 / 2) - 1) : id

        try (ByteArrayOutputStream writer = new ByteArrayOutputStream()) {
            writer.write(Util.encodeIntegral(version))
            writer.write(Util.encodeIntegral(id))

            try (ByteArrayOutputStream rest = new ByteArrayOutputStream()) {
                rest.write(type)
                rest.write(Util.encodeIntegral(time))

                byte[] offset = Util.encodeIntegral(rest.size())

                rest.write(data)

                writer.write(Util.encodeIntegral(rest.size() + offset.length - 1))

                writer.write(offset)
                rest.writeTo(writer)
            }

            result = writer.toByteArray()
        }

        result
    }

    void doCall(Object... args) {}

    @Override
    String toString() {
        return "Packet{" +
                "sender=" + sender +
                ", version=" + version +
                ", time=" + time +
                ", id=" + id +
                ", user=" + user +
                ", type=" + Integer.toBinaryString(type) +
                ", data=" + Arrays.toString(data as byte[]) +
                ", data=" + (data as byte[]).encodeHex() +
                '}'
    }


}
