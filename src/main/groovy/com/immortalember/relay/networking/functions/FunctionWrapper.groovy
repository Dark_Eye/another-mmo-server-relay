package com.immortalember.relay.networking.functions

class FunctionWrapper {
    Closure function
    boolean needsAck

    FunctionWrapper(Closure function, boolean needsAck) {
        this.function = function
        this.needsAck = needsAck
    }
}
