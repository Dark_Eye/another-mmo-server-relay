package com.immortalember.relay.networking.functions

import com.immortalember.relay.Statics
import com.immortalember.relay.Util
import com.immortalember.relay.ai.AIUtility
import com.immortalember.relay.annotations.GlobalFunctionDiscovery
import com.immortalember.relay.annotations.ParameterField
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.db.SearchFunctions
import com.immortalember.relay.packets.Packet
import groovy.util.logging.Slf4j
import groovyx.gpars.actor.Actor

import java.nio.file.Paths

import static groovyx.gpars.actor.Actors.actor

@Slf4j
class GeneralNetworkedFunctions {
    @GlobalFunctionDiscovery("Authorize")
    public static Actor AUTHORIZE = actor {
        loop {
            react {
                log.info("Authorizing {}", it)

                Packet pkt = it.packet
                Statics.server.userDetailsLookup.put(UUID.fromString(pkt.user.uuid), [user: pkt.user, sender: pkt.sender])

                reply()
            }
        }
    }

    @GlobalFunctionDiscovery(value = "Character Listing", returnType = UUID[])
    public static Actor CHARACTER_LISTING = actor {
        loop {
            react {
                reply(SearchFunctions.findUserCharacters(UUID.fromString(it.packet.user.uuid)))
            }
        }
    }

    @GlobalFunctionDiscovery(value = "Spawn Player", parameters = [
            @ParameterField(name = "characterID", type = UUID)
    ])
    public static Actor SPAWN_PLAYER = actor {
        loop {
            react {
                log.info("Spawning player {}", it.characterID)

                Statics.server.spawningPlayers.add(it.characterID)
                Statics.server.userCharacterLookup.put(it.packet.user.uuid, it.characterID)

                reply()
            }
        }
    }

    @GlobalFunctionDiscovery(value = "Spawn Entity", sendAck = false, isCommand = true, parameters = [
            @ParameterField(name = "spawnable", type = String)
    ])
    public static Actor SPAWN_ENTITY = actor {
        loop {
            react {
                log.info("Spawning {}", Util.decodeString(it.spawnable))

                Map<String, String> spw = Statics.JSON_PARSER.parse(Paths.get(Statics.DATA_DIR + "/entity/" + Util.decodeString(it.spawnable) + ".json").toFile())

                IServerIdentifiable ent = Class<IServerIdentifiable>.forName(spw.entity).newInstance(new Object[0])

                Statics.server.registerObject(ent)
                Statics.server.markEntityDirty(ent, Packet.Type.OBJECT_CREATE, ent.FIELD_MAPPING.collect({ k, v -> k }))
                Statics.server.aiHost.registerAI(ent, AIUtility.lookupAI(spw.ai))

                reply()
            }
        }
    }

    @GlobalFunctionDiscovery(value = "Stop Server", sendAck = false, isCommand = true)
    public static Actor STOP_SERVER = actor {
        loop {
            react {
                Statics.isRunning = false
            }
        }
    }
}
