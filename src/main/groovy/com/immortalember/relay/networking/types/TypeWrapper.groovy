package com.immortalember.relay.networking.types


import com.immortalember.relay.Util
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.packets.ObjectPacket
import groovy.transform.ToString
import groovy.util.logging.Slf4j

import java.nio.ByteBuffer

@Slf4j
@ToString(includeNames = true, includeFields = true, allProperties = false)
class TypeWrapper {
    Class id
    boolean hasCreate

    TypeWrapper(Class<? extends IServerIdentifiable> id, boolean hasCreate) {
        this.id = id
        this.hasCreate = hasCreate
    }

    UUID create(ObjectPacket packet, Set<Integer> createdFields = null) {
        IServerIdentifiable s = Util.readType(id, ByteBuffer.wrap(packet.data), createdFields)
        if (hasCreate) s = s.create(s, packet)
        s.ID
    }

//    static UUID create(ObjectPacket data) {
//        log.info("CREATING {}", data)
//        PlayerCharacter me = new PlayerCharacter()
//
//        println(me.FIELD_MAPPING)
//
//        return me.id = Statics.server.registerObject(me)
//    }
//
//    static UUID update(ObjectPacket data) {
//        log.info("UPDATING {}", data)
//        PlayerCharacter me = new PlayerCharacter()
//
//        println(me.FIELD_MAPPING)
//
//        return me.id
//    }
}
