package com.immortalember.relay.networking.types

import groovy.transform.ToString

@ToString(includeNames = true, includeFields = true, allProperties = false)
class TypeFieldWrapper {
    public final Closure set, get
    public final int type
    public final Class trueType

    TypeFieldWrapper(Closure set, Closure get, int type, Class trueType) {
        this.set = set
        this.get = get
        this.type = type
        this.trueType = trueType
    }
}
