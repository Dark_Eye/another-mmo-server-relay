package com.immortalember.relay.data

import com.immortalember.relay.annotations.PacketField
import com.immortalember.relay.annotations.TypeDiscovery
import com.immortalember.relay.db.DBField
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.db.ServerDBData
import groovy.transform.ToString
import groovy.util.logging.Slf4j

@Slf4j
@TypeDiscovery
@ToString(includeNames = true, includeFields = true, allProperties = false)
class ServerTransform extends ServerDBData {
    @PacketField
    public float positionX
    @PacketField
    public float positionY
    @PacketField
    public float positionZ = 202
    @PacketField
    public float rotationX
    @PacketField
    public float rotationY
    @PacketField
    public float rotationZ
    @PacketField
    public float scaleX = 1
    @PacketField
    public float scaleY = 1
    @PacketField
    public float scaleZ = 1
    @PacketField
    @DBField(dbType = "",  relationShipName = "Data")
    public UUID[] data = new UUID[0]
}
