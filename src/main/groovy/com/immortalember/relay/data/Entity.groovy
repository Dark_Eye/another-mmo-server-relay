package com.immortalember.relay.data

import com.immortalember.relay.annotations.PacketField
import com.immortalember.relay.annotations.TypeDiscovery

@TypeDiscovery
class Entity extends ServerTransform {
    @PacketField
    public int HP
    @PacketField
    public int maxHP
    @PacketField
    public boolean focusable = true

    
}
