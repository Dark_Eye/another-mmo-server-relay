package com.immortalember.relay.data

import com.immortalember.relay.annotations.PacketField
import com.immortalember.relay.annotations.TypeDiscovery
import com.immortalember.relay.db.DBField
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.db.ServerDBData
import com.immortalember.relay.packets.ObjectPacket
import groovy.transform.ToString
import groovy.util.logging.Slf4j

@Slf4j
@TypeDiscovery
@ToString(includeNames = true, includeFields = true, allProperties = false, includeSuper = true)
class PlayerCharacter extends Entity {
    @PacketField
    public String firstName
    @PacketField
    public String lastName
    @PacketField
    public byte model
    @PacketField
    @DBField(dbType="User", relationShipName="Owner")
    public UUID player
    @PacketField
    public int nanites = 200
    @PacketField
    public int maxNanites = 200

    PlayerCharacter() {
        HP = maxHP = 2000
    }

    public static IServerIdentifiable create(IServerIdentifiable data, ObjectPacket packet) {
        (data as PlayerCharacter).player = UUID.fromString(packet.user.uuid)
        ServerDBData.create(data, packet)
    }
}
