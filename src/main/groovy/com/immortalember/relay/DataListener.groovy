package com.immortalember.relay

import groovy.util.logging.Slf4j
import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelOption
import io.netty.channel.ChannelPipeline
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioDatagramChannel
import io.netty.handler.logging.LogLevel
import io.netty.handler.logging.LoggingHandler

@Slf4j
class DataListener {
    DataListener(int port) {
        NioEventLoopGroup group = new NioEventLoopGroup()
        try {
            Bootstrap b = new Bootstrap()
            b.group(group).channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .option(ChannelOption.SO_REUSEADDR, true)
                    .handler(new LoggingHandler(LogLevel.DEBUG))
                    .handler({ ch ->
                ChannelPipeline p = ch.pipeline()
                p.addLast(new UserDatapacket())
            } as ChannelInitializer<NioDatagramChannel>)

            log.info("Starting on {}:{}", InetAddress.getByName(Statics.CONFIG.getString("hosting")), port)

            b.bind(InetAddress.getByName(Statics.CONFIG.getString("hosting")), port).sync().channel().closeFuture().await()
        } finally {
            group.shutdownGracefully()
        }
    }
}
