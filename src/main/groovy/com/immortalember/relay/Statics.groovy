package com.immortalember.relay

import com.goterl.lazycode.lazysodium.LazySodiumJava
import com.goterl.lazycode.lazysodium.SodiumJava
import com.goterl.lazycode.lazysodium.interfaces.SecretBox
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.networking.functions.FunctionWrapper
import com.immortalember.relay.networking.types.TypeWrapper
import com.immortalember.relay.packets.AckPacket
import com.immortalember.relay.packets.FunctionPacket
import com.immortalember.relay.packets.Packet
import com.immortalember.relay.server.Server
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import groovyx.gpars.actor.Actor
import groovyx.gpars.dataflow.Promise
import org.neo4j.driver.v1.AuthTokens
import org.neo4j.driver.v1.Driver
import org.neo4j.driver.v1.GraphDatabase
import redis.clients.jedis.JedisPool
import redis.clients.jedis.JedisPoolConfig

import java.lang.reflect.Field
import java.nio.ByteBuffer
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

import static groovyx.gpars.actor.Actors.actor

@Slf4j
class Statics {
    static JsonSlurper JSON_PARSER = new JsonSlurper()

    static final String BASE_DIR = System.getProperty("user.dir")

    static final String DATA_DIR = BASE_DIR + "/data"

    static final String FUNCTIONS_FILE_PATH = BASE_DIR + "/functions.json"
    static final String TYPES_FILE_PATH = BASE_DIR + "/types.json"

    static boolean isRunning = true

    static JedisPool REDIS = new JedisPool(new JedisPoolConfig(), "localhost")
    static final Config CONFIG = ConfigFactory.load()
    static final SecretBox.Lazy SODIUM = new LazySodiumJava(new SodiumJava())
    static volatile Map<Long, FunctionWrapper> functionMapping = [:]
    static volatile Map<Long, TypeWrapper> typeMapping = [:]
    static volatile Map<Class, Long> classMapping = [
            (Float)   : 0,
            (Byte) : 1,
            (Character) : 1,
            (Short) : 1,
            (Integer) : 1,
            (Long) : 1,
            (byte[])  : 2,
            (Object[]): 3,
            (List)    : 4,
            (UUID)    : 5
    ]

    static volatile Driver DB_CONNECTION = GraphDatabase.driver(CONFIG.getConfig("neo4j").getString("url"), AuthTokens.basic(CONFIG.getConfig("neo4j").getString("user"), CONFIG.getConfig("neo4j").getString("password")))

    static Server server = new Server()

    static final float WAIT_TIME = (1000 / CONFIG.getDouble("tick rate")) * 1000000

    static OutputBuffer outputBuffer = new OutputBuffer()

    static List<Actor> INCOMING_DATA_PRE_PROCESSORS = new ArrayList<>([
            // Decompress data using GZIP
            actor {
                loop {
                    react {
                        try {
                            def sender = it.sender
                            byte[] data = it.data

                            if (CONFIG.getBoolean("compress")) {
                                try (ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                     GZIPOutputStream stream = new GZIPOutputStream(bytes)) {
                                    stream.write(data)
                                    stream.finish()

                                    data = bytes.toByteArray()
                                }
                            }

                            reply([sender: sender, data: data])
                        } catch (def e) {
                            log.info("Invalid data received (compression) {}", e)
                        }
                    }
                }
            },
            // Look up user on redis and get data
            actor {
                loop {
                    react {
                        try {
                            def sender = it.sender
                            byte[] data = it.data

                            def wrapped = ByteBuffer.wrap data

                            byte[] raw = new byte[4]
                            wrapped.get(raw)

                            String id = Base64.getEncoder().encodeToString(raw)
                            def dt = []
                            def jedis = null

                            try {
                                jedis = REDIS.getResource()
                                if (jedis.get(id) != null) {
                                    def db = JSON_PARSER.parseText jedis.get(id)

                                    byte[] wrapping = new byte[wrapped.remaining()]
                                    wrapped.get(wrapping)

                                    dt = [sender: sender, db: db, data: wrapping, original: data, redis: id]
                                }
                            } finally {
                                jedis?.close()
                            }
                            reply dt

                        } catch (def e) {
                            log.info("Invalid data received (user lookup) {}", e)
                        }
                    }
                }
            },
            // Decrypt data if encryption is enabled
            actor {
                loop {
                    react {
                        try {
                            byte[] data = it.data
                            ByteBuffer wrapping = ByteBuffer.wrap(data)
                            if (CONFIG.getBoolean("encrypt")) {
                                byte[] nonce = new byte[24]
                                wrapping.get(nonce)

                                byte[] keyBytes = Base64.getDecoder().decode(it.db.client.toString())
                                byte[] cipherBytes = new byte[wrapping.remaining()]
                                wrapping.get(cipherBytes)
                                byte[] messageBytes = new byte[cipherBytes.length - SecretBox.MACBYTES]

                                if (SODIUM.cryptoSecretBoxOpenEasy(messageBytes, cipherBytes, cipherBytes.length, nonce, keyBytes)) {
                                    it.data = messageBytes
                                }
                            }

                            reply it
                        } catch (def e) {
                            log.info("Invalid data received (decryption) {}", e)
                        }
                    }
                }
            },
            // Split internal packets
            actor {
                loop {
                    react {
                        try {
                            ByteBuffer data = ByteBuffer.wrap(it.data as byte[])

                            do {
                                Packet packet = Util.parsePacket(data)

                                packet.sender = it.sender
                                packet.original = it.data
                                packet.user = it.db

                                reply packet
                            } while (data.hasRemaining())
                        } catch (def e) {
                            log.info("Invalid data received (packet splitting)", e)
                        }
                    }
                }
            }
    ])

    static List<Actor> OUTGOING_DATA_POST_PROCESSORS = new ArrayList<>([
            actor {
                loop {
                    react {
                        ByteArrayOutputStream packets = new ByteArrayOutputStream()
                        for (Packet details : it.packets) {
                            byte[] data = details.serialize()
                            if (packets.size() + data.length > 400) {
//                                log.info("Finishing packet and sending")
                                reply([destination: it.destination, key: details.user.server, data: packets.toByteArray()])
                                packets = new ByteArrayOutputStream()
                            }
                            packets.write(data)
                        }

                        if (packets.size() > 0) {
                            reply([destination: it.destination, key: it.packets.get(0).user.server, data: packets.toByteArray()])
//                            log.info("Sent all packets")
                        }
                    }
                }
            },
            actor {
                loop {
                    react {
                        try {
                            byte[] data = it.data
//                            log.info("{} {}", data.encodeHex(), data)
                            if (CONFIG.getBoolean("encrypt")) {
                                try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                                    byte[] nonce = SODIUM.nonce(24)

                                    byte[] keyBytes = Base64.getDecoder().decode(it.key.toString())
                                    byte[] cipherBytes = data
                                    byte[] messageBytes = new byte[cipherBytes.length + SecretBox.MACBYTES]

                                    if (SODIUM.cryptoSecretBoxEasy(messageBytes, cipherBytes, cipherBytes.length, nonce, keyBytes)) {
//                                        log.info("Encrypted data {} {} {}", nonce.encodeHex(), messageBytes.encodeHex(), keyBytes.encodeHex())
                                        out.write(nonce)
                                        out.write(messageBytes)
                                        it.data = out.toByteArray()
                                    }
                                }
                            }

                            reply it
                        } catch (def e) {
                            log.info("Invalid data received (encryption) {}", e)
                        }
                    }
                }
            },
            actor {
                loop {
                    react {
                        try {
                            byte[] data = it.data

                            if (CONFIG.getBoolean("compress")) {
                                try (ByteArrayOutputStream comp = new ByteArrayOutputStream();
                                     ByteArrayInputStream bytes = new ByteArrayInputStream(data);
                                     GZIPInputStream stream = new GZIPInputStream(bytes)) {
                                    byte[] buffer = new byte[1024]
                                    int len
                                    while ((len = stream.read(buffer)) != -1) {
                                        comp.write(buffer, 0, len)
                                    }

                                    it.data = comp.toByteArray()
                                }
                            }

                            reply it
                        } catch (def e) {
                            log.info("Invalid data received (compression) {}", e)
                        }
                    }
                }
            },
            actor {
                DatagramSocket outSocket = new DatagramSocket(null as InetSocketAddress)
                outSocket.setReuseAddress(true)
                DatagramPacket packet = new DatagramPacket(new byte[0], 0)

                loop {
                    react {
                        try {
                            if (!outSocket.isBound()) {
                                outSocket.bind(new InetSocketAddress(Statics.CONFIG.getInt("port")))
                            }

                            //log.info("Sending data {}", it.data.encodeHex())
                            packet.setSocketAddress(it.destination as InetSocketAddress)
                            packet.setData(it.data as byte[])
                            outSocket.send(packet)
                        } catch (def e) {
                            log.info("Invalid data received (sending) {}", e)
                        }
                    }
                }
            }
    ])

    static {
        log.info("Loading Settings")

        Statics.JSON_PARSER.parse(new File(Statics.TYPES_FILE_PATH)).each { k, v ->
            k = Long.parseLong(k as String)
            if (k < 10) return
            Class<? extends IServerIdentifiable> cl = Class<? extends IServerIdentifiable>.forName(v.path + '.' + v.name)
            log.info("Registering type: {} {}", cl, v.create)
            typeMapping.put(k, new TypeWrapper(cl, v.create))
            classMapping.put(cl, k)
        }

        JSON_PARSER.parse(new File(FUNCTIONS_FILE_PATH)).each { k, v ->
            Class cl = Class.forName(v.class)
            Closure func
            if (v.actor) {
                Field field = cl.getField(v.method)
                Actor value = field.get(null) as Actor

                if (value == null) {
                    log.info("MISSING Actor field {}", v)
                    return
                }

                func = { Object... argz ->
                    Map<String, Object> params = [packet: argz[0], args: argz]
                    if (v?.parameters) {
                        ByteBuffer z = ByteBuffer.wrap(argz[0].data)
//                        Util.parseIntegral(z) // Drop packet type
                        v.parameters.each { index, it ->
                             // Drop field location, sending and receiving in the same order
                            def h = v.parameters[Util.parseIntegral(z).toString()]
                            params.put(h.name, Util.readValue(h.typeId, z))
                        }
                    }

                    Promise pr
                    if (argz.length > 0) {
                        pr = value.sendAndPromise(params)
                    } else {
                        pr = value.sendAndPromise()
                    }
                    if (v.requiresAck) {
                        pr.then {
                            if (!(it instanceof AckPacket)) {
                                (argz[0] as FunctionPacket).sendAck(it)
                            }
                        }
                    }
                }
            } else {
                def method = cl.getMethod(v.method as String)
                func = { Object... argz ->

                    List<Object> args = [argz[0]]

                    if (v?.paramters) {
                        ByteBuffer z = ByteBuffer.wrap(argz[0].data)
                        v.paramters.each {
                            args.add(Util.readValue(Integer.parseInt(it.typeId), z))
                        }
                    }

                    if (argz.length > 1) {
                        args.addAll(argz[1, -1])
                    }

                    def it = method.invoke(null, args.toArray())
                    if (!(it instanceof AckPacket)) {
                        (argz[0] as FunctionPacket).sendAck(it)
                    }
                }
            }

            functionMapping.put(Long.parseLong(k as String), new FunctionWrapper(func, v.requiresAck as Boolean))
        }
    }
}
