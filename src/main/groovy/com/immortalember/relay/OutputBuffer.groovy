package com.immortalember.relay

import com.immortalember.relay.packets.Packet
import groovy.util.logging.Slf4j

import java.util.concurrent.locks.ReentrantLock

@Slf4j
class OutputBuffer {
    Map<SocketAddress, List<Packet>> buffer = new HashMap<>()
    private ReentrantLock lock = new ReentrantLock()

    void addPacket(Packet packet) {
        lock.lock()
        try {
            List<Packet> tmp = buffer.getOrDefault(packet.sender, new ArrayList<>())
            tmp.add(packet)
            buffer.put(packet.sender, tmp)
        } finally {
            lock.unlock()
        }
    }

    Map<SocketAddress, List<Packet>> swap() {
        lock.lock()
        Map<SocketAddress, List<Packet>> lag = buffer
        try {
            buffer = new HashMap<>()
        } finally {
            lock.unlock()
        }
        return lag
    }

    void dispatchPackets() {
        Map<SocketAddress, List<Packet>> packetsC = swap()
        Iterator<?> iterator = packetsC.entrySet().iterator()
        while (iterator.hasNext()) {
            Map.Entry<SocketAddress, List<Packet>> pair = iterator.next()
//            log.info("Out packet {}", pair.value)
            loopThrough([destination: pair.key, packets: pair.value])
        }
    }

    protected loopThrough(def data, int id = 0) {
        Statics.OUTGOING_DATA_POST_PROCESSORS.get(id).sendAndContinue(data, {
            int newId = id + 1

            if (Statics.OUTGOING_DATA_POST_PROCESSORS.size() > newId) {
                loopThrough(it, newId)
            }
        })
    }
}
