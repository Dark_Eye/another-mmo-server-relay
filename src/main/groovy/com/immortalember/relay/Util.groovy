package com.immortalember.relay

import com.immortalember.relay.annotations.PacketField
import com.immortalember.relay.db.DBField
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.networking.types.TypeFieldWrapper
import com.immortalember.relay.packets.Packet
import groovy.util.logging.Slf4j

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.nio.ByteBuffer

@Slf4j
class Util {

    static long parseIntegral(ByteBuffer buffer, def counter = [count: 0]) {
        long result = 0

        int last
        boolean first = true
        do {
            int size = 7
            last = Byte.toUnsignedInt(buffer.get())
            if (first) {
                size = 6
            }

            if (first && (last & 0x40) != 0) {
                result = -1
            }

            int mask = (1 << size) - 1
            result = (result << size) + (last & mask)
            if (counter?.count != null) {
                counter.count++
            }

            first = false
        } while ((last & 0x80) != 0)

        result
    }

    static byte[] encodeIntegral(long value) {
        int done = value < 0 ? -1 : 0
        byte[] result

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            boolean firstPass = true
            int shiftCount
            do {
                shiftCount = 7
                if ((value >> 6) == done) {
                    shiftCount = 6
                }

                int section = (int) (value & ((1 << shiftCount) - 1))

                if ((value >> 6) == -1 && done == -1) {
                    section |= 0x40
                }

                value >>= shiftCount
                if (!firstPass) {
                    section |= 0x80
                }
                out.write(section)

                firstPass = false
            } while (shiftCount  != 6)
            result = out.toByteArray()
            reverseArray(result)
        }
        result
    }

    static final byte[] intToByteArray(int value) {
        return new byte[]{
                (byte) (value >>> 24),
                (byte) (value >>> 16),
                (byte) (value >>> 8),
                (byte) value
        }
    }

    static void reverseArray(def result) {
        for (int i = 0; i < result.length / 2; i++) {
            def temp = result[i]
            result[i] = result[result.length - 1 - i]
            result[result.length - 1 - i] = temp
        }
    }

    static Packet parsePacket(ByteBuffer data) {
        Packet packet = new Packet()

        packet.version = (int) parseIntegral(data)
        packet.id = (int) parseIntegral(data)

        int size = (int) parseIntegral(data)
        byte[] d = new byte[size]
        data.get(d)

        ByteBuffer pdata = ByteBuffer.wrap(d)

        int offset = (int) parseIntegral(pdata)

        def sigh = [count: 1]

        packet.type = (Byte.toUnsignedInt(pdata.get()))
        packet.time = parseIntegral(pdata, sigh)
        offset -= sigh.count

        while (offset-- > 0) {
            data.get()
        }

        packet.data = new byte[pdata.remaining()]
        pdata.get(packet.data)

        Packet.PACKET_TYPE_LOOKUP.get(packet.type).newInstance([packet] as Object[])
    }

    static String decodeString(byte[] data) {
        new String(data, "UTF-8")
    }

    static byte[] encodeString(String str) {
        str.getBytes("UTF-8")
    }

    static boolean decodeBoolean(int value) {
        value != 0
    }
    
    static int encodeBoolean(boolean value) {
        value ? 1 : 0
    }

    static byte[] decodeByteArray(ByteBuffer buffer) {
        long size = parseIntegral(buffer)
        byte[] result = new byte[size]
        buffer.get(result)
        return result
    }

    static void encodeByteArray(OutputStream out, byte[] array) {
        DataOutputStream buffer = new DataOutputStream(out)
        buffer.write(encodeIntegral(array.length))
        buffer.write(array)
    }

    static Object readValue(int type, ByteBuffer buffer) {
        switch (type) {
            case Statics.classMapping.get(Float):
                buffer.getFloat()
                break
            case Statics.classMapping.get(Integer):
                parseIntegral(buffer)
                break
            case Statics.classMapping.get(byte[]):
                decodeByteArray(buffer)
                break
            case Statics.classMapping.get(Object[]):
                //subtype
                //count
                //data (loop read value into list) (subtype, buffer)
                int subtype = parseIntegral(buffer)
                int size = parseIntegral(buffer)
                Object[] arr = new Object[size]
                for (int i = 0; i < size; i++) {
                    arr[i] = readValue(subtype, buffer)
                }
                arr
                break
            case Statics.classMapping.get(List):
                //count
                //data (loop read value into list) (parseInt, buffer)
                // > el type
                // > el data
                int size = parseIntegral(buffer)
                List arr = new ArrayList(size)
                for (int i = 0; i < size; i++) {
                    int subtype = parseIntegral(buffer)
                    arr.add(readValue(subtype, buffer))
                }
                arr
                break
            case Statics.classMapping.get(UUID):
                new UUID(buffer.getLong(), buffer.getLong())
                break
            case 6:
            case 7:
            case 8:
            case 9:
                log.error("Not yet implemented: {}",type)
                throw new IllegalStateException("Use of reserved type ${type}")
            default:
                readType(Statics.typeMapping.get(type), buffer)
                break
        }
    }

    static <T extends IServerIdentifiable> T readType(T newObject, ByteBuffer buffer, Set<Integer> updatedFields = null) {
        while (buffer.hasRemaining()) {
            int id = parseIntegral(buffer)
            updatedFields?.add(id)
            TypeFieldWrapper wrapper = newObject.FIELD_MAPPING.get(id)
            wrapper.set(readValue(wrapper.type, buffer))
        }
        newObject
    }

    static <T extends IServerIdentifiable> T readType(Class<T> id, ByteBuffer buffer, Set<Integer> updatedFields = null) {
        IServerIdentifiable newObject = readType(id.newInstance(new Object[0]), buffer, updatedFields)
        Statics.server.registerObject(newObject)
        (T)newObject
    }

    static void writeValue(OutputStream out, Object value, boolean writeType = true) {
        Class cl = value.getClass().isArray() ? Object[] : value.getClass()
        writeValue((int)Statics.classMapping.get(cl), out, value, writeType)
    }

    static void writeValue(int type, OutputStream out, Object value, boolean writeType = true) {
        DataOutputStream buffer = new DataOutputStream(out)
        if (value == null && writeType) {
            buffer.write(encodeIntegral(6))
            return
        }
        if (writeType) buffer.write(encodeIntegral(type as long))
        switch (type) {
            case Statics.classMapping.get(Float):
                buffer.writeFloat(value as float)
                break
            case Statics.classMapping.get(Integer):
                buffer.write(encodeIntegral(value as long))
                break
            case Statics.classMapping.get(byte[]):
                encodeByteArray(buffer, value as byte[])
                break
            case Statics.classMapping.get(Object[]):
                Class cType = value.getClass().getComponentType()
                cType = cType.isArray() ? Object[] : cType
                int subType = Statics.classMapping.get(cType)
                buffer.write(encodeIntegral(subType))
                buffer.write(encodeIntegral(value.length))
                value.each {
                    writeValue(subType, out, it, false)
                }
                break
            case Statics.classMapping.get(List):
                buffer.write(encodeIntegral(value.length))
                value.each {
                    writeValue(out, it)
                }
                log.warn("UNTESTED: {}", type)
                break
            case Statics.classMapping.get(UUID):
                buffer.writeLong(value.mostSignificantBits)
                buffer.writeLong(value.leastSignificantBits)
                break
            case 6:
                log.warn("Writing null: {}", type)
                break
            case 7:
            case 8:
            case 9:
                log.error("Use of reserved type: {}", type)
                throw new IllegalStateException("Use of reserved type ${type}")
            default:
                writeObject(buffer, value)
                break
        }
    }

    static void writeObject(OutputStream buffer, IServerIdentifiable value, boolean writeType = false, Collection<Integer> fields = null) {
        value.FIELD_MAPPING.each { k, v ->
            if (fields == null || fields.contains((Integer) k)) {
                buffer.write(encodeIntegral(k))
                writeValue(v.type, buffer, v.get(), writeType)
            }
        }
    }

    public static List<Field> getDBFields(Class<?> type) {
        List<Field> result = new ArrayList<Field>()

        while (type != null && type != Object.class) {
            type.declaredFields.each {
                if (((!Modifier.isTransient(it.modifiers) && Modifier.isPublic(it.modifiers)) || it.annotations.find {
                    it.annotationType() == DBField
                } != null)
                        && !Modifier.isStatic(it.modifiers)
                        && !Modifier.isFinal(it.modifiers)
                        && !it.isSynthetic()) {
                    result.add(it)
                }
            }
            type = type.getSuperclass()
        }

        result
    }

    public static List<Field> getPacketFields(Class<?> type) {
        List<Field> result = new ArrayList<Field>()

        while (type != null && type != Object.class) {
            type.declaredFields.each {
                if (((!Modifier.isTransient(it.modifiers) && Modifier.isPublic(it.modifiers)) || it.annotations.find {
                    it.annotationType() == PacketField
                } != null)
                        && !Modifier.isStatic(it.modifiers)
                        && !Modifier.isFinal(it.modifiers)
                        && !it.isSynthetic()) {
                    result.add(it)
                }
            }

            type = type.getSuperclass()
        }

        result
    }
}
