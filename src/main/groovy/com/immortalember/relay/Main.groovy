package com.immortalember.relay

import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.networking.functions.FunctionWrapper
import com.immortalember.relay.networking.types.TypeWrapper
import com.immortalember.relay.packets.AckPacket
import com.immortalember.relay.packets.FunctionPacket
import groovy.util.logging.Slf4j
import groovyx.gpars.actor.Actor
import groovyx.gpars.dataflow.Promise

@Slf4j
class Main {
    static {
        try {
            new File(System.getenv("TEMP")+ "/libsodium").deleteDir()
        } catch (e) {}
    }

    static void main(String... args) {
        log.info "DataListener is starting"

        new Thread(Statics.server, "Game Server").start()

        new DataListener(Statics.CONFIG.getInt("port"))
    }
}
