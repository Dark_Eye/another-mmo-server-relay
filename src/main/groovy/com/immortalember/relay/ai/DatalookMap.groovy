package com.immortalember.relay.ai

import com.immortalember.relay.Statics
import com.immortalember.relay.db.IServerIdentifiable

import java.util.regex.Matcher
import java.util.regex.Pattern

class DatalookMap {
    private static final Pattern pattern = Pattern.compile("^\\s*(\\w+)\\.(.*)\\s*")
    Map<String, Object> consts
    Map<String, String> values
    Map<String, Object> external

    IServerIdentifiable boundOwner

    DatalookMap(Map<String, Object> consts, Map<String, String> values) {
        this.consts = consts ?: new HashMap<>()
        this.values = values ?: new HashMap<>()
    }

    void bind(IServerIdentifiable boundOwner, Map<String, Object> external) {
        this.boundOwner = boundOwner
        this.external = external
    }

    void unbind() {
        boundOwner = null
    }

    void set(String key, Object obj) {
        external.put(key, obj)
    }

    Object get(String key) {
        if (key == null) return null
        if (external?.containsKey(key)) {
            return external.get(key)
        } else if (consts?.containsKey(key)) {
            return consts.get(key)
        } else if (values?.containsKey(key)) {
            String lookup = values.get(key)
            Matcher matcher = pattern.matcher(lookup)
            if (matcher.find()) {
                String object = matcher.group(1)
                String field = matcher.group(2)
                switch (object) {
                    case "Owner":
                        return getProperty(boundOwner, field)
                    case "Statics":
                        return getProperty(Statics, field)
                }
            }
        }
        null
    }

    Object getOrDefault(String key, Object defa) {
        get(key) ?: defa
    }

    static Object getProperty(Object object, String property) {
        property.tokenize('.').inject object, { obj, prop ->
            obj[prop]
        }
    }

}
