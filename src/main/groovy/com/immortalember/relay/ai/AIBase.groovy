package com.immortalember.relay.ai

import com.immortalember.relay.db.IServerIdentifiable
import groovy.transform.ToString

@ToString(includeNames = true, includeFields = true, allProperties = false)
class AIBase {
    String name
    Class<IServerIdentifiable> entityBase
    AIComponent component
    DatalookMap blackboard

    public Set<Integer> doAction(IServerIdentifiable obj, float deltaTime) {
        if (!entityBase.isInstance(obj)) return null
        Set<Integer> keys = component.doAction(obj, blackboard, deltaTime) ?: new HashSet<>()

        return keys
    }
}
