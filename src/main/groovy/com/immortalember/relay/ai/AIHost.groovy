package com.immortalember.relay.ai

import com.immortalember.relay.data.ServerTransform
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.packets.Packet
import com.immortalember.relay.server.Server

class AIHost {
    Server server
    Map<IServerIdentifiable, AIBase> ais = new WeakHashMap<>()
    Map<IServerIdentifiable, Map<String, Object>> customProperties = new WeakHashMap<>()

    public AIHost(Server server) {
        this.server = server
    }

    public synchronized void doRun(float deltaTime) {
        ais.removeAll { k, v ->
            v.blackboard.bind(k, customProperties.get(k))
            Set<Integer> result = v.doAction(k, deltaTime)
            if (result == null) {
                return true
            } else {
                if (k instanceof ServerTransform && !result.isEmpty()) {
                    server.markEntityDirty(k, Packet.Type.OBJECT_UPDATE, result)
                }
                return false
            }
        }
    }

    public synchronized void registerAI(IServerIdentifiable obj, AIBase ai) {
        ais.put(obj, ai)
        customProperties.put(obj, new HashMap<>())
    }
}
