package com.immortalember.relay.ai.components

import com.immortalember.relay.ai.AIComponent
import com.immortalember.relay.ai.DatalookMap
import com.immortalember.relay.data.ServerTransform
import com.immortalember.relay.db.IServerIdentifiable
import com.immortalember.relay.util.Matrix4x4f
import com.immortalember.relay.util.Vector

class MoveToAIComponent extends AIComponent {
    String points
    String speed
    String lastPoint = "__lastPoint"

    @Override
    Set<Integer> doAction(IServerIdentifiable obj, DatalookMap blackboard, float deltaTime) {
        if (obj instanceof ServerTransform) {
            int lastPointValue = blackboard.getOrDefault(lastPoint, 0)
            float movementSpeed = blackboard.getOrDefault(speed, 0.5f) * deltaTime
            List<Map<String, Float>> points = blackboard.get(points)

            if (points?.size() > 0) {
                Map<String, Float> point = points.get(lastPointValue)
                float x = point.x - obj.positionX
                float y = point.y - obj.positionY
                float z = point.z - obj.positionZ

                Vector difX = new Vector(x, y, z)
                difX.normalize()
                Vector difY = difX.crossProduct(0, 0, 1)
                difY.normalize()
                Vector difZ = difY.crossProduct(difX)

                obj.rotationY = Math.atan2(difX.z, Math.sqrt(difX.x * difX.x + difX.y * difX.y)) * 180.0f / Math.PI
                obj.rotationZ = Math.atan2(difX.y, difX.x) * 180.0f / Math.PI
                obj.rotationX = 0 //Math.atan2() * 180.0f / Math.PI

                float max = Math.max(Math.max(Math.abs(x), Math.abs(y)), Math.abs(z))

                float nX = max == 0 ? 0 : (x / max) * movementSpeed
                float nY = max == 0 ? 0 : (y / max) * movementSpeed
                float nZ = max == 0 ? 0 : (z / max) * movementSpeed

                nX = Math.abs(nX) < Math.abs(x) ? nX : x
                nY = Math.abs(nY) < Math.abs(y) ? nY : y
                nZ = Math.abs(nZ) < Math.abs(z) ? nZ : z

                obj.positionX += nX
                obj.positionY += nY
                obj.positionZ += nZ

                if (obj.positionX == point.x && obj.positionY == point.y && obj.positionZ == point.z) {
                    lastPointValue += 1
                    if (lastPointValue >= points.size()) {
                        lastPointValue = 0
                    }
                    blackboard.set(lastPoint, lastPointValue)
                }

                return new HashSet<Integer>([0, 1, 2, 3, 4, 5])
            }
        }
        return null
    }
}
