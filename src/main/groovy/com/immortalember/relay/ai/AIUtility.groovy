package com.immortalember.relay.ai

import com.immortalember.relay.Statics
import groovy.util.logging.Slf4j

import java.io.File
import java.nio.file.Paths
import java.nio.file.Path

@Slf4j
class AIUtility {
    static Map<String, AIBase> aiLookup = [:]

    static String AI_DIR = Statics.DATA_DIR + "/ai"

    static {
        File dir = Paths.get(AI_DIR).toFile()
        dir.mkdirs()

        dir.eachFile {
            def a = Statics.JSON_PARSER.parse(it)
            if (a instanceof List) {
                a.each {addAI(it)}
            } else {
                addAI(a)
            }
        }

    }

    public static AIBase lookupAI(String name) {
        aiLookup.get(name)
    }

    private static void addAI(def m) {
        AIBase base = new AIBase()

        base.name = m.name
        base.entityBase = Class.forName(m.entity)
        base.component = Class.forName(m.component.class).newInstance(new Object[0])

        m.component?.parameters?.each {  k, v ->
            base.component[k] = v
        }

        base.blackboard = new DatalookMap(m.data?.constant, m.data?.dynamic)

        log.info("Found AI {}", base)
        aiLookup.put(base.name, base)
    }
}
